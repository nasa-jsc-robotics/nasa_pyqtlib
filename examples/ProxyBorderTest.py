#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import ProxyBorder

from GraphSceneViewMain import GraphSceneViewMain

class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)


        self.widget = QtGui.QWidget()

        button1 = QtGui.QPushButton('button1')
        button2 = QtGui.QPushButton('button2')
        lineEdit = QtGui.QLineEdit()
        layout = QtGui.QVBoxLayout()
        layout.addWidget(button1)
        layout.addWidget(button2)

        self.widget.setLayout(layout)

        self.proxyBorder = ProxyBorder()


        self.proxy = QtGui.QGraphicsProxyWidget()
        self.proxy.setWidget(self.widget)
        self.proxyBorder.setProxy(self.proxy)

        self.scene.addItem(self.proxyBorder)


        self.addBoxButton = QtGui.QPushButton('add box')
        self.addBoxButton.clicked.connect(self.addBox)

        layout = self.getMainLayout()
        layout.addWidget(self.addBoxButton)

    def addBox(self):
        box = BoxItem()
        box.color = QtGui.QColor(QtCore.Qt.green)
        self.scene.addItem(box)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
