#!/usr/bin/env python

"""
Example application for the LogFileView widget.

This app is meant to give a code example and quick checkout of the LogFileView widget.
This app will "tail" syslog while writing a new log entry every second.  While the
category and message are static, the level will rotate through the various levels.
"""

import time
import logging
import nasa_common_logging
import os

from pyqtlib import QtCore, QtGui

from pyqtlib import LogFileView

level = [
    logging.DEBUG,
    logging.INFO,
    logging.WARN,
    logging.ERROR]

LOGFILEVIEW_LOGFILE = os.path.join(os.environ['HOME'],'.log','logfileview.log')


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.logger = logging.getLogger('LogFileViewApp')

        self.widget = LogFileView()
        self.widget.addFile(LOGFILEVIEW_LOGFILE)
        self.setCentralWidget(self.widget)

        self.i = 0

        #Start the update timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(1000)

        self.show()

    def update(self):
        """
        Log a new message.
        """
        l = level[self.i % len(level)]
        self.i = self.i + 1
        self.logger.log(l, 'Test message')


def main():
    import signal, sys

    # Configure nasa_common_logging with the defaults
    nasa_common_logging.configure_common_logging(level='DEBUG', handlers=['syslog','console','file'],file_path=LOGFILEVIEW_LOGFILE)

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()