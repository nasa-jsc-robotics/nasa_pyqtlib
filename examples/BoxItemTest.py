#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import BoxItem

from GraphSceneViewMain import GraphSceneViewMain

class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.addBoxButton = QtGui.QPushButton('add box')
        self.addBoxButton.clicked.connect(self.addBox)

        layout = self.getMainLayout()
        layout.addWidget(self.addBoxButton)

    def addBox(self):
        box = BoxItem()
        box.color = QtGui.QColor(QtCore.Qt.green)
        self.scene.addItem(box)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
