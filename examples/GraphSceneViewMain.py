from pyqtlib import QtGui, QtCore
import sys
from pyqtlib.node_graph import GraphScene, GraphView
from pprint import pprint, pformat


class GraphSceneViewMain(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.statusBar().show()

        self.scene = GraphScene()
        self.view = GraphView()
        self.view.setScene(self.scene)

        self.originVLine = QtGui.QGraphicsLineItem(0.0, -50.0, 0.0, 50.0)
        self.originHLine = QtGui.QGraphicsLineItem(-50.0, 0.0, 50.0, 0.0)
        self.scene.addItem(self.originHLine)
        self.scene.addItem(self.originVLine)

        self.originVisibleCheckbox = QtGui.QCheckBox('Show origin')
        self.originVisibleCheckbox.stateChanged.connect(self.originVisibleCheckboxChanged)
        self.originVisibleCheckbox.setChecked(True)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateView)
        self.timer.start(30)

        self.mainWidget = QtGui.QWidget()
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.addWidget(self.view)
        self.mainLayout.addWidget(self.originVisibleCheckbox)

        self.selectedItemInfoBox = QtGui.QPlainTextEdit()
        self.selectedItemInfoDock = QtGui.QDockWidget()
        self.selectedItemInfoDock.setWidget(self.selectedItemInfoBox)

        self.undoView = QtGui.QUndoView(self.scene.getUndoStack())
        self.undoDock = QtGui.QDockWidget('Undo')
        self.undoDock.setWidget(self.undoView)

        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.selectedItemInfoDock)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.undoDock)

        self.mainWidget.setLayout(self.mainLayout)
        self.setCentralWidget(self.mainWidget)

        self.scene.selectionChanged.connect(self.updateItemInfo)

    def updateItemInfo(self):
        itemInfo = []
        selectedItems = self.scene.selectedItems()
        for item in selectedItems:
            scenePos = item.scenePos()
            viewPos  = self.view.mapFromScene(scenePos)
            globalPos = self.view.viewport().mapToGlobal(viewPos)

            info = {
                'class' : item.__class__.__name__,
                'scenePos' : {
                    'x' : scenePos.x(),
                    'y' : scenePos.y()
                },
                'viewPos': {
                    'x': viewPos.x(),
                    'y': viewPos.y()
                },
                'globalPos': {
                    'x': globalPos.x(),
                    'y': globalPos.y()
                },
                'boundingRect' : {
                    'x' : item.boundingRect().x(),
                    'y' : item.boundingRect().y(),
                    'w' : item.boundingRect().width(),
                    'h' : item.boundingRect().height()
                }
            }
            itemInfo.append(info)

        self.selectedItemInfoBox.setPlainText(pformat(itemInfo, indent=1, width=30))


    def updateView(self):
        self.updateStatusBar()
        self.updateItemInfo()

    def updateStatusBar(self, event=None):
        mouseGlobal = QtGui.QCursor.pos()
        mouseView  = self.view.mapFromGlobal(mouseGlobal)
        mouseScene = self.view.mapToScene(mouseView)

        mg = 'global: x={:9.3f},y={:9.3f}'.format(mouseGlobal.x(), mouseGlobal.y())
        mv = 'view: x={:9.3f},y={:9.3f}'.format(mouseView.x(), mouseView.y())
        ms = 'scene: x={:9.3f},y={:9.3f}'.format(mouseScene.x(), mouseScene.y())
        self.statusBar().showMessage('{} {} {}'.format(mg,mv,ms))

    def getMainLayout(self):
        return self.mainLayout

    def originVisibleCheckboxChanged(self, state):
        self.originHLine.setVisible(state)
        self.originVLine.setVisible(state)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = GraphSceneViewMain()
    ex.show()
    ex.resize(1200,600)
    sys.exit(app.exec_())
