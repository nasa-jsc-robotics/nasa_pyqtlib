#!/usr/bin/env python
import logging
import argparse
import sys

from pyqtlib import QtCore, QtGui

import nasa_common_logging
from pyqtlib import LogFileView
from pyqtlib import PathManagerWidget

from pyqtlib import LogInputWidget

styleSheet = """
    * {
    font-size: 12px;
    font-family: "Courier New";
    }
"""

class MainWindow(QtGui.QMainWindow):

    def __init__(self, logLevel='INFO', parent=None):
        super(MainWindow, self).__init__(parent)

        self.logInputWidget = LogInputWidget()

        self.logWindow = LogFileView(parent=self)
        self.logWindow.setFilterLogLevel(LogFileView.LogLevels[logLevel])

        self.pathManager = PathManagerWidget()
        self.pathManager.pathAdded.connect(self.logWindow.addFile)
        self.pathManager.pathRemoved.connect(self.logWindow.removeFile)
        self.pathManager.setVisible(False)
        
        self.pathManager.addPath('/var/log/nasa_common_logging.log')
        #self.pathManager.addPath('/var/log/syslog')

        managePaths = QtGui.QAction('Manage &Paths', self)
        managePaths.triggered.connect(self.showPathManager)

        settingsMenu = QtGui.QMenu('Settings', self)
        settingsMenu.addAction(managePaths)
        
        menuBar = self.menuBar()
        menuBar.setNativeMenuBar(False)
        menuBar.addMenu(settingsMenu)

        self.logInputWidgetDock = QtGui.QDockWidget()
        self.logInputWidgetDock.setWidget(self.logInputWidget)
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.logInputWidgetDock)
        
        self.setCentralWidget(self.logWindow)

    def showPathManager(self):
        self.pathManager.setVisible(True)

def main(logLevel='INFO'):
    import signal, sys

    nasa_common_logging.configure_common_logging(level='DEBUG')

    app = QtGui.QApplication([])
    app.setStyleSheet(styleSheet)
    main = MainWindow(logLevel)
    main.show()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Simple Syslog viewer')
    parser.add_argument('-l', '--level', type=str, choices={'DEBUG', 'INFO', 'WARNING', 'ERROR'}, default='INFO', help='Log level.  Default = "INFO"')


    args, unknown_args = parser.parse_known_args(sys.argv[1:])
    main(args.level)