#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import Node, BoxNode, CirclePort, RequiresPort, ProvidesPort, PortOrientation, GraphView, GraphScene

from GraphSceneViewMain import GraphSceneViewMain

class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.lineEdit = QtGui.QLineEdit()
        self.addBoxNodeButton = QtGui.QPushButton('add BoxNode')
        self.addCirclePortButton = QtGui.QPushButton('add CirclePort')
        self.addProvidesPortButton = QtGui.QPushButton('add ProvidesPort')
        self.addRequiresPortButton = QtGui.QPushButton('add RequiresPort')
        self.removePortButton = QtGui.QPushButton('remove port')

        self.checkbox = QtGui.QCheckBox('output port')
        self.checkbox.setChecked(False)

        self.connectToggle = QtGui.QCheckBox('connect mode')
        self.connectToggle.setChecked(False)
        self.connectToggle.stateChanged.connect(self.updateMouseMode)

        self.addBoxNodeButton.clicked.connect(self.addBoxNode)
        self.addCirclePortButton.clicked.connect(self.addCirclePort)
        self.addProvidesPortButton.clicked.connect(self.addProvidesPort)
        self.addRequiresPortButton.clicked.connect(self.addRequiresPort)
        self.removePortButton.clicked.connect(self.removePort)


        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(self.addBoxNodeButton)
        hLayout.addWidget(self.checkbox)
        hLayout.addWidget(self.connectToggle)
        #hLayout.addWidget(self.lineEdit)
        hLayout.addStretch()

        layout = self.getMainLayout()
        layout.addWidget(self.view)
        layout.addLayout(hLayout)
        layout.addWidget(self.addBoxNodeButton)
        layout.addWidget(self.addCirclePortButton)
        layout.addWidget(self.addProvidesPortButton)
        layout.addWidget(self.addRequiresPortButton)
        layout.addWidget(self.removePortButton)

        self.count = 0
        self.node_count = 0
        self.output = True

    def addBoxNode(self):
        node = BoxNode('BoxNode', 'Node{}'.format(self.node_count))
        self.node_count += 1
        self.scene.addItem(node)

    def getSelectedNode(self):
        selectedItems = self.scene.selectedItems()
        if len(selectedItems) > 0:
            item = selectedItems[0]
            if isinstance(item,BoxNode):
                return item
        return None

    def getOrientation(self):
        if self.checkbox.isChecked():
            return PortOrientation.Right
        else:
            return PortOrientation.Left


    def addCirclePort(self):
        port = CirclePort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1

        node = self.getSelectedNode()
        if node is not None:
            node.addPort(port)
        else:
            self.scene.addItem(port)


    def addProvidesPort(self):
        port = ProvidesPort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1
        node = self.getSelectedNode()
        if node is not None:
            node.addPort(port)
        else:
            self.scene.addItem(port)

    def addRequiresPort(self):
        port = RequiresPort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1
        node = self.getSelectedNode()
        if node is not None:
            node.addPort(port)
        else:
            self.scene.addItem(port)

    def updateMouseMode(self, value):
        if self.connectToggle.isChecked():
            self.scene.setMouseModeConnect()
        else:
            self.scene.setMouseModePointer()

    def removePort(self):
        node = self.getSelectedNode()
        if node is not None:
            name = node.getPortNames()[0]
            node.removePort(name)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
