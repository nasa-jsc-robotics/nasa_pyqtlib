#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import CirclePort
from pyqtlib.node_graph.MultiStyleConnection import MultiStyleConnection


from GraphSceneViewMain import GraphSceneViewMain

definition1 = {
    'style': MultiStyleConnection.ConnectionStyle.OrthoLine,
    'handleMoved': True,
    'handlePos': [350, 25]
}


class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.lineEdit = QtGui.QLineEdit()

        self.addCirclePortButton = QtGui.QPushButton('add CirclePort')
        self.deserializeSelectedButton = QtGui.QPushButton('deserialize selected')

        self.scene.setConnectionType(MultiStyleConnection)

        self.connectToggle = QtGui.QCheckBox('connect mode')
        self.connectToggle.setChecked(False)
        self.connectToggle.stateChanged.connect(self.updateMouseMode)

        self.addCirclePortButton.clicked.connect(self.addCirclePort)
        self.deserializeSelectedButton.clicked.connect(self.deserializeSelected)

        self.lineButton = QtGui.QPushButton('Line')
        self.lineButton.setCheckable(True)
        self.lineButton.clicked.connect(self.setConnectionStyleLine)
        self.lineButton.setChecked(True)

        self.orthoButton = QtGui.QPushButton('OrthoLine')
        self.orthoButton.setCheckable(True)
        self.orthoButton.clicked.connect(self.setConnectionStyleOrthoLine)

        self.splineButton = QtGui.QPushButton('Spline')
        self.splineButton.setCheckable(True)
        self.splineButton.clicked.connect(self.setConnectionStyleSpline)

        self.styleGroup = QtGui.QButtonGroup()
        self.styleGroup.addButton(self.lineButton)
        self.styleGroup.addButton(self.orthoButton)
        self.styleGroup.addButton(self.splineButton)
        self.styleGroup.setExclusive(True)

        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(self.connectToggle)
        hLayout.addWidget(self.lineButton)
        hLayout.addWidget(self.orthoButton)
        hLayout.addWidget(self.splineButton)
        hLayout.addStretch()

        layout = self.getMainLayout()
        layout.addWidget(self.view)
        layout.addLayout(hLayout)

        layout.addWidget(self.addCirclePortButton)
        layout.addWidget(self.deserializeSelectedButton)

        self.count = 0
        self.node_count = 0
        self.output = True

        self.addCirclePort(QtCore.QPointF(100, 0))
        self.addCirclePort(QtCore.QPointF(400, 150))

    def getSelectedNode(self):
        selectedItems = self.scene.selectedItems()
        if len(selectedItems) > 0:
            item = selectedItems[0]
            return item
        return None

    @QtCore.pyqtSlot()
    def addCirclePort(self, pos=None):
        port = CirclePort('port_{}'.format(self.count))
        self.scene.addItem(port)
        self.count += 1
        if pos is not None:
            port.setPos(pos)

    def updateMouseMode(self, value):
        if self.connectToggle.isChecked():
            self.scene.setMouseModeConnect()
        else:
            self.scene.setMouseModePointer()

    def deserializeSelected(self):
        item = self.getSelectedNode()
        if isinstance(item, MultiStyleConnection):
            item.deserialize(definition1)

    def setConnectionStyleLine(self):
        self.scene.setNewConnectionKwargs({'style': MultiStyleConnection.ConnectionStyle.Line})

    def setConnectionStyleOrthoLine(self):
        self.scene.setNewConnectionKwargs({'style': MultiStyleConnection.ConnectionStyle.OrthoLine})

    def setConnectionStyleSpline(self):
        self.scene.setNewConnectionKwargs({'style': MultiStyleConnection.ConnectionStyle.Spline})




if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
