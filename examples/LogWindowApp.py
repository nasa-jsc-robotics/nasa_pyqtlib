#!/usr/bin/env python
from pyqtlib import QtCore, QtGui
import logging

from pyqtlib import LogWindow

class MainWindow(QtGui.QMainWindow):

	def __init__(self, parent=None):
		super(MainWindow, self).__init__(parent)

		self.logWindow = LogWindow()
		self.logWindow.setLogLevel(logging.DEBUG)	
		self.setCentralWidget(self.logWindow)

		# Start the update timer
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.updateView)
		self.timer.start(1000)

		self.show()

	def updateView(self):
		logging.debug('debug')
		logging.info('info')
		logging.warning('warning')
		logging.error('error')


def main():
	import signal, sys
	
	app = QtGui.QApplication([])
	main = MainWindow()

	signal.signal(signal.SIGINT, signal.SIG_DFL)
	sys.exit(app.exec_())

if __name__ == "__main__":
	main()