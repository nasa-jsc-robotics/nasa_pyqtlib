#!/usr/bin/env python
from pyqtlib import QtCore, QtGui

from pyqtlib import PathManagerWidget

class AllFiles(QtGui.QMainWindow):

	def __init__(self, parent=None):
		super(AllFiles, self).__init__(parent)

		self.setWindowTitle('All Files')
		self.widget = PathManagerWidget()	
		self.setCentralWidget(self.widget)
		self.show()

class OnlyDirs(QtGui.QMainWindow):

	def __init__(self, parent=None):
		super(OnlyDirs, self).__init__(parent)

		self.setWindowTitle('Only Dirs')
		self.widget = PathManagerWidget(dirs=True)	
		self.setCentralWidget(self.widget)
		self.show()

class FilterPyFiles(QtGui.QMainWindow):

	def __init__(self, parent=None):
		super(FilterPyFiles, self).__init__(parent)

		self.setWindowTitle('FilterPyFiles')
		self.widget = PathManagerWidget(filters=['*.py'])	
		self.setCentralWidget(self.widget)
		self.show()

class AllFilesDialog(QtGui.QMainWindow):

	def __init__(self, parent=None):
		super(AllFilesDialog, self).__init__(parent)

		self.setWindowTitle('AllFilesDialog')
		self.widget = PathManagerWidget(parent=self)
		self.widget.setVisible(False)
		self.widget.setWindowTitle('Files')
		self.widget.setWindowModality(QtCore.Qt.WindowModal)

		self.pushButton = QtGui.QPushButton('Edit settings')
		self.pushButton.clicked.connect(self.showDialog)
		self.setCentralWidget(self.pushButton)
		self.show()

	def showDialog(self):
		self.widget.setVisible(True)


def main():
	import signal, sys
	
	app = QtGui.QApplication([])
	main1 = AllFiles()
	main1.setGeometry(0,0, 300, 300)
	main2 = OnlyDirs()
	main2.setGeometry(0,500, 300, 300)
	main3 = FilterPyFiles()
	main3.setGeometry(500,0, 300, 300)
	main4 = AllFilesDialog()
	main4.setGeometry(500,500, 300, 300)

	signal.signal(signal.SIGINT, signal.SIG_DFL)
	sys.exit(app.exec_())

if __name__ == "__main__":
	main()