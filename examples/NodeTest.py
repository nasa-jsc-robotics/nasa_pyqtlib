#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import Node, BoxNode, CirclePort, RequiresPort, ProvidesPort, PortOrientation

from GraphSceneViewMain import GraphSceneViewMain

class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)


        self.addPortButton = QtGui.QPushButton('add port')
        self.addProvidesPortButton = QtGui.QPushButton('add provides port')
        self.addRequiresPortButton = QtGui.QPushButton('add requires port')
        self.removePortButton = QtGui.QPushButton('remove port')

        self.inputCheckBox = QtGui.QCheckBox('input')
        self.outputCheckBox = QtGui.QCheckBox('output')
        self.outputCheckBox.setChecked(True)

        self.checkboxGroup = QtGui.QButtonGroup()
        self.checkboxGroup.addButton(self.inputCheckBox)
        self.checkboxGroup.addButton(self.outputCheckBox)

        self.checkboxGroup.setExclusive(True)


        self.addPortButton.clicked.connect(self.addPort)
        self.addProvidesPortButton.clicked.connect(self.addProvidesPort)
        self.addRequiresPortButton.clicked.connect(self.addRequiresPort)
        self.removePortButton.clicked.connect(self.removePort)

        self.node = BoxNode('typeName', 'instanceName')
        self.scene.addItem(self.node)

        checkLayout = QtGui.QHBoxLayout()
        checkLayout.addWidget(self.addPortButton)
        checkLayout.addWidget(self.inputCheckBox)
        checkLayout.addWidget(self.outputCheckBox)

        layout = self.getMainLayout()
        layout.addLayout(checkLayout)
        layout.addWidget(self.addProvidesPortButton)
        layout.addWidget(self.addRequiresPortButton)
        layout.addWidget(self.removePortButton)

        self.count = 0
        self.output = True

    def getSelectedNode(self):
        selectedItems = self.scene.selectedItems()
        if len(selectedItems) > 0:
            item = selectedItems[0]
            if isinstance(item,BoxNode):
                return item
        return None

    def getOrientation(self):
        if self.outputCheckBox.isChecked():
            return PortOrientation.Right
        else:
            return PortOrientation.Left

    def addPort(self):
        port = CirclePort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1

        node = self.getSelectedNode()
        if node is not None:
            node.addPort(port)

    def addProvidesPort(self):
        port = ProvidesPort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1
        node = self.getSelectedNode()
        if node is not None:
            node.addPort(port)

    def addRequiresPort(self):
        port = RequiresPort('port_{}'.format(self.count), orientation=self.getOrientation())

        if self.output:
            self.output = False
        else:
            self.output = True

        self.count += 1
        node = self.getSelectedNode()
        if node is not None:
            node.addPort(port)

    def removePort(self):
        node = self.getSelectedNode()
        if node is not None:
            name = node.getPortNames()[0]
            node.removePort(name)





if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
