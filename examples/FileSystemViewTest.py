#!/usr/bin/env python
from pyqtlib import QtCore, QtGui

from pyqtlib import FileSystemView

import os


PATH = os.path.join(os.path.dirname(__file__),'test_root')
FILE_EXTENSION = '.group'

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        model = QtGui.QFileSystemModel()
        model.setNameFilters(['*.py','*' + FILE_EXTENSION])
        model.setNameFilterDisables(False)
        model.setRootPath(PATH)


        self.view = FileSystemView()
        self.view.setModel(model)
        self.view.setRootIndex(model.index(PATH))
        self.view.setDragEnabled(True)
        self.view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.view.customContextMenuRequested.connect(self.customContextMenuRequested)

        self.setCentralWidget(self.view)
        self.show()

    def customContextMenuRequested(self, pos):
        menu = QtGui.QMenu()
        fileMenu = self.view.getFileMenu()
        fileMenu.setTitle('File')
        viewMenu = self.view.getViewMenu()
        viewMenu.setTitle('View')

        menu.addMenu(fileMenu)
        menu.addMenu(viewMenu)
        menu.exec_(self.mapToGlobal(pos))


def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()