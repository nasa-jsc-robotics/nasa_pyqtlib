from pyqtlib import QtGui, QtCore

DATA_ROLE = QtCore.Qt.UserRole + 1

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.model = QtGui.QStandardItemModel()
        self.view = QtGui.QTreeView()

        self.view.setModel(self.model)

        self.setup(self.model)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.timeUp)
        self.timer.start(1000)

        self.setCentralWidget(self.view)
        self.show()

    def setup(self, model):

        self.item1 = QtGui.QStandardItem()
        self.item1.setCheckable(True)
        self.model.dataChanged.connect(self.dataChanged)
        self.item2 = QtGui.QStandardItem()
        self.item3 = QtGui.QStandardItem()

        self.item1.setData(1.234,QtCore.Qt.DisplayRole)
        self.item1.setData('splot',QtCore.Qt.DisplayRole)
        self.item2.setData(0xA001,QtCore.Qt.DisplayRole)


        self.model.appendRow([self.item1,self.item2, self.item3])

    def timeUp(self):
        a = self.model.item(0,0).data(QtCore.Qt.DisplayRole)
        b = self.model.item(0,1).data(QtCore.Qt.DisplayRole)
        c = self.model.item(0,2).data(QtCore.Qt.DisplayRole)

        print 'a({})={} b({})={} c({})={}'.format(type(a), a, type(b), b, type(c), c)

    def dataChanged(self, *args, **kwargs):
        print 'something got checked: {}, {}'.format(args,kwargs)

def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()