from pyqtlib import QtGui, QtCore
from pyqtlib import DictionaryWidget


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.dictionaryWidget = DictionaryWidget()
        self.dictionaryWidget.setDictionary({'root': 'value'})

        self.getExpansionButton = QtGui.QPushButton('Get expansion state')
        self.setExpansionButton = QtGui.QPushButton('Set expansion state')
        self.dictionaryRefreshedButton = QtGui.QPushButton('Dictionary Refreshed')
        self.getExpansionButton.clicked.connect(self.getExpansionState)
        self.setExpansionButton.clicked.connect(self.setExpansionState)
        self.dictionaryRefreshedButton.clicked.connect(self.dictionaryRefresh)

        self.expansionState = None

        self.widget = QtGui.QWidget()
        self.layout = QtGui.QVBoxLayout()
        self.layout.addWidget(self.dictionaryWidget)
        self.layout.addWidget(self.getExpansionButton)
        self.layout.addWidget(self.setExpansionButton)
        self.layout.addWidget(self.dictionaryRefreshedButton)
        self.widget.setLayout(self.layout)

        self.setCentralWidget(self.widget)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.status)
        self.timer.start(1000)

    def getExpansionState(self):
        self.expansionState = self.dictionaryWidget.view.getExpandedStates()

    def setExpansionState(self):
        self.dictionaryWidget.view.setExpandedStates(self.expansionState)

    def dictionaryRefresh(self):
        self.dictionaryWidget.view.dictionaryRefreshed()

    def status(self):
        print self.dictionaryWidget.getDictionary()




def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()
    main.show()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()