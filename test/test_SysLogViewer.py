from nasa_common_logging import configure_common_logging
import logging
import traceback

logger = logging.getLogger(__name__)

if __name__ == "__main__":

    configure_common_logging()

    try:
        a = 1. / 0.
    except ZeroDivisionError as e:
        trace = traceback.format_exc()
        logger.error(r'error:{}'.format(str(trace)))
