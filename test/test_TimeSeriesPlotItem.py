#!/usr/bin/env python
import unittest
import random
from pyqtlib import QtGui, QtCore
import pyqtgraph
import numpy as np

from pyqtlib.plotter.TimeSeriesPlotWidget import TimeSeriesPlotItem


class TestTimeSeriesPlotItem(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.app = QtGui.QApplication([])

    @classmethod
    def tearDownClass(cls):
        cls.app.quit()

    def setUp(self):
        self.testPlotWidget = pyqtgraph.PlotWidget()
        self.testPlot = self.testPlotWidget.plot()
        self.value = 0

    def tearDown(self):
        del self.testPlotWidget

    def constructDefaults(self):
        return (TimeSeriesPlotItem(name='testPlot', parent_plot_items=[self.testPlot]))

    def testCallbackIncrement(self):
        r = self.value
        self.value = self.value + 1
        return r

    def testCallBack(self):
        return random.random()

    def test_defaults(self):
        """
        Test the item using default constructor arguments
        """
        testDataPlotterItem = self.constructDefaults()
        testTime, testData = testDataPlotterItem.getValues()

        self.assertEqual(len(testData),0)
        self.assertEqual(len(testTime),0)

    def test_updateAndClear(self):
        """
        Test the update callback, clear, and the max_size
        """
        testDataPlotterItem = TimeSeriesPlotItem(name='testPlot',
                                                 parent_plot_items=[self.testPlot],
                                                 update_callback=self.testCallbackIncrement,
                                                 max_size=4)

        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),0)
        self.assertEqual(len(testTime),0)

        testDataPlotterItem.sampleData()
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),1)
        self.assertEqual(len(testTime),1)
        self.assertEqual(testData[-1],0)

        testDataPlotterItem.sampleData()
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),2)
        self.assertEqual(len(testTime),2)
        self.assertEqual(testData[-1],1)

        testDataPlotterItem.sampleData()
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),3)
        self.assertEqual(len(testTime),3)
        self.assertEqual(testData[-1],2)

        testDataPlotterItem.sampleData()
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),4)
        self.assertEqual(len(testTime),4)
        self.assertEqual(testData[-1],3)

        # test max size: Add another element, and test that there are still only 4 elements
        testDataPlotterItem.sampleData()
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),4)
        self.assertEqual(len(testTime),4)
        self.assertEqual(testData[-1],4)
        self.assertEqual(testData[0],1)

        # Test clear: clear and test the size of the data is zero
        testDataPlotterItem.clear()
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),0)
        self.assertEqual(len(testTime),0)

    def test_setData(self):
        """
        Test setting the data and appending values
        """
        testDataPlotterItem = self.constructDefaults()

        data = [1,2,3,4,5]
        time = [0.1,0.2,0.3,0.4,0.5]
        dt = np.column_stack((time,data))

        # Set the data, and test if equal to inputs
        testDataPlotterItem.setValues(dt)
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(data,testData)
        self.assertEqual(time,testTime)

        # start appending data
        testDataPlotterItem.appendValue((0.6,6))
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),6)
        self.assertEqual(len(testTime),6)
        self.assertEqual(testData[-1],6)
        self.assertEqual(testTime[-1],0.6)

        # append data out of time order
        testDataPlotterItem.appendValue((0.2,7))
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),7)
        self.assertEqual(len(testTime),7)
        self.assertEqual(testData[-1],7)
        self.assertEqual(testTime[-1],0.2)


    def test_appendData(self):
        testDataPlotterItem = self.constructDefaults()

        testDataPlotterItem.appendData(0)
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),1)
        self.assertEqual(len(testTime),1)
        self.assertEqual(testData[-1],0)

        testDataPlotterItem.appendData(1)
        testTime, testData = testDataPlotterItem.getValues()
        self.assertEqual(len(testData),2)
        self.assertEqual(len(testTime),2)
        self.assertEqual(testData[-1],1)
        self.assertTrue(testTime[0] < testTime[1])

# class TestDataPlotterWidget(unittest.TestCase):
#
# 	def setUp(self):
# 		self.app = QtGui.QApplication([])
# 		self.dataPlotterWidget = DataPlotterWidget()
# 		self.value = 0
#
# 	def tearDown(self):
# 		pass
#
# 	def createDataSet(self, start_time, time_step, offset, points):
# 		data = []
# 		time = []
# 		for i in range(points):
# 			data.append(random.random() + offset)
# 			time.append(start_time + i * time_step)
# 		return (data,time)


if __name__ == '__main__':
    unittest.main()