#!/usr/bin/env python
import sys
import unittest
from pyqtlib import QtGui, QtCore

from pyqtlib import PathWidget

class FixtureWidget(QtGui.QWidget):
    sigExpandAll = QtCore.pyqtSignal()
    sigAdjustColumns = QtCore.pyqtSignal()

class TestPathWidget(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.app = QtGui.QApplication(sys.argv)

    @classmethod
    def tearDownClass(cls):
        cls.app.quit()

    def setUp(self):
        self.fixture = FixtureWidget()
        self.widget = PathWidget(delimiters='/')
        self.widget.resize(800,600)
        self.fixture.sigExpandAll.connect(self.widget.expandAll)
        self.fixture.sigAdjustColumns.connect(self.widget.adjustColumns)
        
        self.expand = self.fixture.sigExpandAll.emit
        self.adjust = self.fixture.sigAdjustColumns.emit
        
    def tearDown(self):
        del self.widget    

    #@unittest.skip('reasons...')
    def test_addPath(self):
        """ Basic add path tests """
        self.widget.addPath('/with/leading/delimiter')

        #/with
        #  /leading
        #    /delimiter
        m = self.widget.model
        self.assertEqual(m.columnCount(),1)
        self.assertEqual(m.rowCount(),1)
        self.assertEqual(str(m.item(0).data(QtCore.Qt.DisplayRole)), '/with')
        self.assertEqual(str(m.item(0).child(0).data(QtCore.Qt.DisplayRole)),'/leading')
        self.assertEqual(str(m.item(0).child(0).child(0).data(QtCore.Qt.DisplayRole)),'/delimiter')

    #@unittest.skip('reasons...')
    def test_addPath_delimiters(self):
        """ More complicated delimiters add path tests """
        m = self.widget.model

        self.widget.addPath('/with.leading.delimiter')
        self.assertEqual(m.columnCount(),1)
        self.assertEqual(m.rowCount(),1)
        self.assertEqual(str(m.item(0).data(QtCore.Qt.DisplayRole)), '/with.leading.delimiter')
        self.assertFalse(m.item(0).hasChildren())

        self.widget.addPath('/with.leading/delimiter')
        self.assertEqual(m.rowCount(), 2)
        self.assertEqual(m.item(0).data(QtCore.Qt.DisplayRole), '/with.leading')
        self.assertEqual(m.item(0).child(0).data(QtCore.Qt.DisplayRole), '/delimiter')

    def test_addPath_customDelimiters(self):
        self.widget = PathWidget(delimiters='.')

        m = self.widget.model

        self.widget.addPath('.with/leading/delimiter')
        self.assertEqual(m.columnCount(),1)
        self.assertEqual(m.rowCount(),1)
        self.assertEqual(str(m.item(0).data(QtCore.Qt.DisplayRole)), '.with/leading/delimiter')
        self.assertFalse(m.item(0).hasChildren())

        self.widget.addPath('.with/leading.delimiter')
        self.assertEqual(m.rowCount(), 2)
        self.assertEqual(m.item(0).data(QtCore.Qt.DisplayRole), '.with/leading')
        self.assertEqual(m.item(0).child(0).data(QtCore.Qt.DisplayRole), '.delimiter')


    #@unittest.skip('reasons...')
    def test_getCheckedItems(self):
        """ Get a list of checked items """
        self.widget = PathWidget(checkable=True)
        self.widget.resize(800,600)
        self.fixture.sigExpandAll.connect(self.widget.expandAll)
        self.fixture.sigAdjustColumns.connect(self.widget.adjustColumns)

        self.widget.addPath('/level1/level2/item1')
        self.widget.addPath('/level1/level2/item2')
        self.widget.addPath('/level1/item3')

        level1Item = self.widget.model.getItem('/level1')
        level2Item = self.widget.model.getItem('/level1/level2')
        item1 = self.widget.model.getItem('/level1/level2/item1')
        item2 = self.widget.model.getItem('/level1/level2/item2')
        self.assertNotEqual(item1, None)
        self.assertNotEqual(item2, None)
        self.assertEqual(level1Item.checkState(),QtCore.Qt.Unchecked)
        self.assertEqual(level2Item.checkState(),QtCore.Qt.Unchecked)
        level1Item.setCheckState(QtCore.Qt.Checked)
        self.assertEqual(level2Item.checkState(),QtCore.Qt.Checked)
        self.assertEqual(item1.checkState(),QtCore.Qt.Checked)
        self.assertEqual(item2.checkState(),QtCore.Qt.Checked)

        paths = self.widget.getCheckedPaths()
        self.assertEqual(len(paths), 3)

        paths = self.widget.getCheckedPaths(leaf_only=False)
        self.assertEqual(len(paths),5)

    #@unittest.skip('reasons...')
    def test_getCheckedPaths(self):
        """ Get a list of checked items """
        self.widget = PathWidget(checkable=True)
        self.widget.resize(800,600)
        self.fixture.sigExpandAll.connect(self.widget.expandAll)
        self.fixture.sigAdjustColumns.connect(self.widget.adjustColumns)

        self.widget.addPath('/level1/level2/item1')
        self.widget.addPath('/level1/level2/item2')
        self.widget.addPath('/level1/item3')

        level1Item = self.widget.model.pathItems['/level1']
        level2Item = self.widget.model.pathItems['/level1/level2']
        item1 = self.widget.model.pathItems['/level1/level2/item1']
        item2 = self.widget.model.pathItems['/level1/level2/item2']
        self.assertNotEqual(item1, None)
        self.assertNotEqual(item2, None)
        self.assertEqual(level1Item.checkState(),QtCore.Qt.Unchecked)
        self.assertEqual(level2Item.checkState(),QtCore.Qt.Unchecked)
        level1Item.setCheckState(QtCore.Qt.Checked)
        self.assertEqual(level2Item.checkState(),QtCore.Qt.Checked)
        self.assertEqual(item1.checkState(),QtCore.Qt.Checked)
        self.assertEqual(item2.checkState(),QtCore.Qt.Checked)

        items = self.widget.getCheckedPaths()
        self.assertEqual(len(items), 3)
        self.assertTrue('/level1/item3' in items)
        self.assertTrue('/level1/level2/item1' in items)
        self.assertTrue('/level1/level2/item2' in items)

        items = self.widget.getCheckedPaths(leaf_only=False)
        self.assertEqual(len(items),5)
        self.assertTrue('/level1/item3' in items)
        self.assertTrue('/level1/level2/item1' in items)
        self.assertTrue('/level1/level2/item2' in items)
        self.assertTrue('/level1' in items)
        self.assertTrue('/level1/level2' in items)

    @unittest.skip('reasons...')
    def test_addPaths(self):
        pass

    @unittest.skip('reasons...')
    def test_getPath(self):
        pass

    @unittest.skip('reasons...')
    def runApp(self):
        '''
        Run this function to start the application for a test case.

        Helpful for debugging.
        '''
        self.expand()
        self.adjust()
        self.app.exec_()
        

if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    unittest.main()