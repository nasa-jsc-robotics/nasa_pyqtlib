import unittest

from pyqtlib import QtGui, QtCore

from pyqtlib.model import ValueItem

class TestValueItem(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_setData_hex16(self):
        item = ValueItem(format='hex16')
        item.setData(0xABCD, role=ValueItem.DataRole)

        self.assertEqual(item.data(ValueItem.DataRole),0xABCD)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '0xABCD')

        item.setData('0xABCD', role=QtCore.Qt.EditRole)
        self.assertEqual(item.data(ValueItem.DataRole),0xABCD)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '0xABCD')

    def test_setData_hex32(self):
        item = ValueItem(format='hex32')
        item.setData(0xABCD1234, role=ValueItem.DataRole)

        self.assertEqual(item.data(ValueItem.DataRole),0xABCD1234)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '0xABCD1234')

        item.setData('0x1234ABCD', role=QtCore.Qt.EditRole)
        self.assertEqual(item.data(ValueItem.DataRole),0x1234ABCD)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '0x1234ABCD')

    def test_setData_hex64(self):
        item = ValueItem(format='hex64')
        item.setData(0xABCD1234, role=ValueItem.DataRole)

        self.assertEqual(item.data(ValueItem.DataRole),0xABCD1234)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '0x00000000ABCD1234')

        item.setData('0x1234ABCD', role=QtCore.Qt.EditRole)
        self.assertEqual(item.data(ValueItem.DataRole),0x1234ABCD)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '0x000000001234ABCD')

    def test_setData_float(self):
        item = ValueItem(format='float')
        item.setData(-1.2345, role=ValueItem.DataRole)

        self.assertEqual(item.data(ValueItem.DataRole),-1.2345)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '-1.2345')

        item.setData('5.4321', role=QtCore.Qt.EditRole)
        self.assertEqual(item.data(ValueItem.DataRole),5.4321)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), ' 5.4321')

    def test_setData_int(self):
        item = ValueItem(format='int')
        item.setData(123, role=ValueItem.DataRole)

        self.assertEqual(item.data(ValueItem.DataRole),123)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '123')

        item.setData('321', role=QtCore.Qt.EditRole)
        self.assertEqual(item.data(ValueItem.DataRole),321)
        self.assertEqual(item.data(QtCore.Qt.DisplayRole), '321')

if __name__ == "__main__":
    unittest.main()