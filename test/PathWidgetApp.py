#!/usr/bin/env python

from pyqtlib import QtGui, QtCore

from pyqtlib.tree import ObjectTreeWidget
from pyqtlib.tree.path_widget import PathWidget
#from pyqtlib.tree.PathDataWidget import PathDataWidget as PathWidget

from pyqtlib.model import AnyChildFilterProxyModel

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.pathDataWidget = PathWidget(checkable=True)
        self.pathDataWidget.pathCheckChanged.connect(self.pathCheckChanged)
        self.pathDataWidget.selectionChanged.connect(self.selectionChanged)
        self.pathDataWidget.pathValueChanged.connect(self.pathValueChanged)

        self.pathDataWidget.model.rowsAboutToBeRemoved.connect(self.rowsAboutToBeRemoved)

        #self.tableView = QtGui.QTableView()
        #self.tableView.setModel(self.pathDataWidget.model)

        self.setup()

        self.debugTree = ObjectTreeWidget()
        dock = QtGui.QDockWidget(self)
        dock.setWidget(self.debugTree)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea,dock)

        self.textBox = QtGui.QLineEdit()
        self.removePathButton = QtGui.QPushButton('Remove path')
        self.addPathButton = QtGui.QPushButton('Add path')

        self.removePathButton.clicked.connect(self.removePath)
        self.addPathButton.clicked.connect(self.addPath)

        widget = QtGui.QWidget()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.pathDataWidget)

        hlayout = QtGui.QHBoxLayout()
        hlayout.addWidget(self.textBox)
        hlayout.addWidget(self.addPathButton)
        hlayout.addWidget(self.removePathButton)

        layout.addLayout(hlayout)

        widget.setLayout(layout)

        # tableDock = QtGui.QDockWidget(self)
        # tableDock.setWidget(self.tableView)
        # self.addDockWidget(QtCore.Qt.RightDockWidgetArea,tableDock)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateDebugTree)
        self.timer.start(100)

        self.setCentralWidget(widget)
        self.show()

    def addPath(self):
        self.pathDataWidget.addPath(str(self.textBox.text()))

    def removePath(self):
        self.pathDataWidget.removePath(str(self.textBox.text()))

    def setup(self):
        self.pathDataWidget.addPath('b')
        self.pathDataWidget.addPath('c')
        self.pathDataWidget.addPath('a/d/e')
        self.pathDataWidget.addPath('a/d/f')

        self.pathDataWidget.setValue('a/d/e', 'col1', 1, 'hex16')
        self.pathDataWidget.setValue('a/d/e', 'col2', 2, 'hex32')
        self.pathDataWidget.setValue('a/d/e', 'col3', 3, 'hex64', color=QtCore.Qt.yellow)

        self.pathDataWidget.setValue('a', 'col1', 1, 'int')
        self.pathDataWidget.setValue('a', 'col2', 'blarg', 'str')
        self.pathDataWidget.setValue('a', 'col4', -1.234, 'float')

    def pathCheckChanged(self, path, checkstate):
        print 'PathCheckChanged: {}->{}'.format(path, 'checked' if checkstate == QtCore.Qt.Checked else 'unchecked')

    def pathValueChanged(self, path):
        print 'PathValueChanged: {}'.format(path)

    def updateDebugTree(self):
        self.debugTree.updateView('data', self.pathDataWidget.getData())
        self.debugTree.expandAll()
        self.debugTree.adjustColumns()

    def selectionChanged(self, path):
        print 'SelectionChanged: {}'.format(path)

    def rowsAboutToBeRemoved(self, *args):
        print 'rowsAboutToBeRemoved:{}'.format(args)


def main():
    import signal, sys

    app = QtGui.QApplication([])
    main = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()