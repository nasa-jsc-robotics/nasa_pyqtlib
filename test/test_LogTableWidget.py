import unittest
import re

from pyqtlib import LogTableWidget, QtCore, QtGui


def setFilterLogLevel(level, showUnmatchedLevels):
    """
    The filter's log level

    Args:
        level: minimum log level to display as an int
    """
    filterLogLevel = level

    showStrings = []
    filterStrings = []
    for levelString, value in LogTableWidget.LogLevels.items():
        if value >= filterLogLevel:
            showStrings.append(levelString+'$')
        else:
            filterStrings.append(levelString+'$')

    if showUnmatchedLevels:
        allFilterStrings = '|'.join(filterStrings)
        regex = '(?!{})'.format(allFilterStrings)
    else:
        allShowStrings = '|'.join(showStrings)
        regex = allShowStrings

    return regex

app = QtGui.QApplication([])


class TestRegex(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_setFilterLogLevel(self):
        regex = setFilterLogLevel(10, False)

        rg = re.compile(regex)
        self.assertEqual(None, rg.match('DEBUG'))
        self.assertEqual(None, rg.match(' '))
        self.assertEqual(None, rg.match('DEBUGGATHON'))
        self.assertNotEqual(None, rg.match('INFO'))

    def test_setFilterLogLevel(self):
        widget = LogTableWidget()
        regex = widget.getFilterRegex(20, True)

        rg = re.compile(regex)
        #print regex
        self.assertEqual(None, rg.match('INFO'))
        self.assertEqual(None, rg.match('DEBUG   '))
        self.assertEqual(None, rg.match('DEBUG'))
        self.assertNotEqual(None, rg.match('DEBUGG'))
        self.assertNotEqual(None, rg.match(' '))
        self.assertNotEqual(None, rg.match(''))
        self.assertNotEqual(None, rg.match('!'))





if __name__ == "__main__":
    unittest.main()