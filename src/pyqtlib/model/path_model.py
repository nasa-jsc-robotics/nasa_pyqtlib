from pyqtlib import QtGui, QtCore
import re

from .value_item import ValueItem

def pathToList(path,delimiters):
    """
    Convert a path to a list of elements

    @param path Input path
    @param delimiters Tokens to split the path
    @return List of path elements
    """
    leading_path = '([^{}]+)'.format(delimiters)
    path_element = '([{}][^{}]+|[{}])'.format(delimiters,delimiters,delimiters)

    pathList = []

    leading_match = re.match(leading_path, path)
    if leading_match:
        pathList.append(leading_match.group(0))

    path_match = re.findall(path_element,path)
    if path_match:
        pathList = pathList + path_match

    return pathList

class PathItem(QtGui.QStandardItem):
    """
    This item stores and displays the full path of the item
    """
    def __init__(self, text, checkable=False):
        super(PathItem, self).__init__(text)
        self.setEditable(False)
        self.setCheckable(checkable)

class PathModel(QtGui.QStandardItemModel):
    """
    The PathModel stores data as a dictionary of data rows.  The key
    is a path-like name, e.g.('/path/to/data') and the value is another dictionary of 
    key-value pairs.  The first column can be set to checkable in the constructor.  The
    data is stored in the model in a hierarchy, with each delimiter building another level
    in the heirarchy (like one might see in a tree view of a file system.)  While this
    model can be used in several of the standard views, it is best used in a QTreeView.
    """
    pathCheckChanged = QtCore.pyqtSignal(str, QtCore.Qt.CheckState)
    pathValueChanged = QtCore.pyqtSignal(str)

    # define role enums to access items in the model
    ValueRole     = ValueItem.DataRole          # The value stored in a ValueItem
    ValueNameRole = ValueItem.DataRole + 1      # The name stored in a ValueItem
    PathRole      = ValueItem.DataRole + 2      # The path stored in a ValueItem or a PathItem

    def __init__(self, checkable=True, delimiters='/', parent=None):
        """
        @type delimiters strings
        @param delimiters Delimiters to denote levels of the hierarchy.
                Note: delimiters are a single string of characters.  Multiple
                      delimiters can be used in the same string, ex: '/.' will
                      delimit on every '/' and every '.'
        """
        QtGui.QStandardItemModel.__init__(self, parent)

        self.pathItems   = {}
        self.modelValues = {}
        self.delimiters = delimiters
        self.itemsCheckable = checkable
        self.itemChanged.connect(self.emitPathChangedSignals)

    def _createPathItem(self, path, name):
        """
        Create and initialize a PathItem
        :param path:
        :param name:
        :return:
        """
        item = PathItem(name, self.itemsCheckable)
        item.setData(path, PathModel.PathRole)
        return item

    def _createValueItem(self, path, name, format='str' ):
        """
        Create and initialize a ValueItem
        :param path: Name of the path
        :param name: Attribute name
        :param format: format string
        :return: ValueItem
        """
        item = ValueItem(format=format)
        item.setData(name, PathModel.ValueNameRole)
        item.setData(path, PathModel.PathRole)
        return item

    def _findChild(self, name, parent):
        """
        Find the child item of a parent by name

        @param name Name of the item
        @param parent The parent item for the search
        """
        for i in range(parent.rowCount()):
            if parent.child(i).data(QtCore.Qt.DisplayRole) == name:
                return parent.child(i)
        return None

    def _getChildPaths(self, parent, leaf_only):
        """
        Return the paths of all child items

        @type parent PathItem
        @param parent Parent Item to check for children

        @type leaf_only bool
        @param leaf_only If true, only return paths that do not have children themselves
        """
        paths = []
        for i in range(parent.rowCount()):
            path = [parent.child(i).data(PathModel.PathRole)]
            child_paths = self._getChildPaths(parent.child(i),leaf_only)
            if (leaf_only and len(child_paths) == 0) or not leaf_only:
                paths += path
            paths += child_paths

        return paths


    def addPath(self, path):
        """
        Add a single path to the model

        @type path string
        @param path Path to add

        @return PathModelItem that was created at the end of the tree
        """
        path = str(path)
        if path not in self.pathItems:
            pathList = pathToList(path, self.delimiters)
            item = None

            parent = self.invisibleRootItem()
            for i in range(len(pathList)):
                path = ''.join(pathList[0:i+1])
                itemName = pathList[i]
                item = self._findChild(itemName, parent)
                if item == None:
                    item = self._createPathItem(path, itemName)
                    self.pathItems[path] = item
                    self.modelValues[path] = {}
                    parent.appendRow(item)
                parent = item
            self.sort(0)
            return item
        return None

    @QtCore.pyqtSlot(str)
    def appendHeaderLabel(self, label):
        """
        Append an addition header column to the tree
        @param label Name of the new column
        """
        self.setHorizontalHeaderItem(self.columnCount(),QtGui.QStandardItem(str(label)))

    def clearAll(self):
        """
        Clear the entire model, headers and data
        """
        self.pathItems.clear()
        self.modelValues.clear()
        self.clear()

    def clearData(self):
        """
        Remove all data from the model
        """
        self.pathItems.clear()
        self.modelValues.clear()

        # Take all the header items
        headerItems=[]
        for col in range(self.columnCount()):
            headerItems.append(self.takeHorizontalHeaderItem(col))

        self.clear()

        # replace all the header items
        for col in range(len(headerItems)):
            self.setHorizontalHeaderItem(col, headerItems[col])

    def getCheckedPaths(self, leaf_only=True):
        """
        Return a list of paths that are currently checked

        @param leaf_only If true, only items with no children will be returned
        """
        paths = []
        for path, item in self.pathItems.items():
            if item.checkState() == QtCore.Qt.Checked:
                if leaf_only:
                    if not item.hasChildren():
                        paths.append(path)
                else:
                    paths.append(path)
        return paths

    def getCheckState(self, path):
        return self.pathItems[path].checkState()

    def getChildPaths(self, path, leaf_only=True):
        return self._getChildPaths(self.getItem(path),leaf_only)

    def getHeaderLabel(self, col):
        """
        Get the label of a column
        @param col Column index
        @return String of the header value
        """
        header = str(self.headerData(col, QtCore.Qt.Horizontal,QtCore.Qt.DisplayRole))
        return header

    def getHeaderLabelIndex(self, label):
        """
        Get the column number of a header label
        @param label Header label to look for
        @return The number of the column, or None if the label does not exist
        """
        for i in range(self.columnCount()):
            if self.getHeaderLabel(i) == str(label):
                return i
        return None

    def getData(self, skip_empty=True):
        """
        Get the values in the table as a nested dictionary.
        :param: skip_empty: If set to true, paths with no values will be omitted
        :return:
        """
        data = {}
        for path, values in self.modelValues.items():
            if values == {} and skip_empty:
                continue
            else:
                data[path] = {}
                for name, item in values.items():
                    v = item.data(ValueItem.DataRole)
                    data[path][name] = v
        return data

    def getPathFromItem(self, item):
        return str(item.data(PathModel.PathRole))

    def getPathFromIndex(self, itemIndex):
        item = self.itemFromIndex(itemIndex)
        if item is None:
            return None
        return self.getPathFromItem(item)

    def getItem(self,path):
        """
        Get a PathModelItem from the model by the path name

        @param path Path of the item
        @return PathModelItem corresponding to the path
        """
        return self.pathItems.get(path,None)

    def getPaths(self):
        return self.pathItems.keys()

    def getUncheckedPaths(self, leaf_only=True):
        """
        Return a list of paths that are currently unchecked

        @param leaf_only If true, only items with no children will be returned
        """
        paths = []
        for path, item in self.pathItemDict.items():
            if item.checkState() == QtCore.Qt.Unchecked:
                if leaf_only:
                    if not item.hasChildren():
                        paths.append(path)
                else:
                    paths.append(path)
        return paths

    @QtCore.pyqtSlot(str, str)
    def getValue(self, path, name=None):
        """
        Get a value from the model

        :param path: Path of the item
        :param name: Name of the value.  This corresponds to the column header
        :return: value from the model
        """
        row = self.modelValues[path]
        if name is not None:
            return row[str(name)].data(ValueItem.DataRole)
        else:
            values = {}
            for name, item in row.items():
                values[name] = item.data(ValueItem.DataRole)
            return values

    def emitPathChangedSignals(self, item):
        path = self.getPathFromItem(item)
        if item.isCheckable():
            self.pathCheckChanged.emit(str(path),item.checkState())
        self.pathValueChanged.emit(str(path))

    def mimeData(self, listIndexes):
        """
        Overload the mimeData

        Mime data is used for drag and drop and the clipboard.  By overloading this
        method, the text is set to the paths that are selected.
        """
        # Call base class to get default mime data
        data = QtGui.QStandardItemModel.mimeData(self, listIndexes)
        # next, set the text mime data to be the full path of the selected item(s)
        # If multiple items, seperate them with a newline character
        text = ''
        for index in listIndexes:
            text = text + self.getPathFromIndex(index)
        data.setText(text)
        return data

    def removePath(self, path):
        """
        Remove a single path from the model.  This will remove all child from the model as well

        @type path string
        @param path Path to add
        """
        path = str(path)
        item = self.getItem(path)

        if item is None:
            return
        row  = item.row()
        parent = item.parent()

        self._removeChildRows(item)

        parentIndex = self.indexFromItem(parent)
        self.removeRows(row,1,parentIndex)

        for pathKey in self.pathItems.keys():
            if  pathKey.startswith(path):
                self.pathItems.pop(pathKey)

        for pathKey in self.modelValues.keys():
            if pathKey.startswith(path):
                self.modelValues.pop(pathKey)

        self.sort(0)

    def _removeChildRows(self,parent):
        for i in range(parent.rowCount()):
            self._removeChildRows(parent.child(0))
            self.removeRows(i, 1, self.indexFromItem(parent))

    @QtCore.pyqtSlot(list)
    def setHeaderLabels(self, labels):
        """
        Set the header of the widget
        @param labels List of strings to put in the widget's header
        """
        self.setHorizontalHeaderLabels(labels)

    @QtCore.pyqtSlot(str, str, QtCore.QVariant)
    def setValue(self, path, name, value, format='str', color=None):
        '''
        Set the value of an item in a particular column.
        If the path or the column does not exist, it will be added.

        @param path Path of the row
        @param name Name of the column.  This cannot be the name of the first column
        @parm value Value to set
        @param format Format of the output.  This must be one of the valid formats described in
                      pyqtlib.model.ValueItem.  NOTE: this is only valid the first time the value
                      is created.
        '''
        path = str(path)
        name = str(name)

        #if path does not exist, add it
        if path not in self.pathItems:
            self.addPath(path)

        item = self.modelValues[path].get(name, None)

        # If item doesn't exist, create a new item
        if item is None:
            pathItem = self.pathItems[path]
            parent = pathItem.parent()
            if parent is None:
                parent = self.invisibleRootItem()
            item = self._createValueItem(path, name, format)
            self.modelValues[path][name] = item

            col = self.getHeaderLabelIndex(name)
            if col is None:
                self.appendHeaderLabel(name)
                col = self.getHeaderLabelIndex(name)
            parent.setChild(pathItem.row(),col,item)

        item.setData(value,ValueItem.DataRole)

        if color is not None:
            item.setData(QtGui.QBrush(color), QtCore.Qt.BackgroundColorRole)
