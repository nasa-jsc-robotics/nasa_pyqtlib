"""
The DictionaryModel is a QStandardItemModel used to model an editable Python dictionary.  This model can be used
 with the DictionaryView widget.  If you don't need to use the model directly, consider using the DictionaryWidget
 instead.
"""
from pyqtlib import QtGui, QtCore

import ast

atomic_types = [str, float, bool, int, unicode]


class DictionaryModelItem(QtGui.QStandardItem):

    ExpandedRole = QtCore.Qt.UserRole + 1

    def __init__(self, *args, **kwargs):
        super(DictionaryModelItem, self).__init__(*args, **kwargs)
        self.expanded = False
        self.foreground_color = QtCore.Qt.white

    def setData(self, value, role):
        if role == DictionaryModelItem.ExpandedRole:
            self.expanded = value
        return super(DictionaryModelItem, self).setData(value, role)

    def data(self, role):
        if role == DictionaryModelItem.ExpandedRole:
            return self.expanded
        else:
            return super(DictionaryModelItem, self).data(role)


class DictionaryModel(QtGui.QStandardItemModel):

    dictionaryRefreshed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(DictionaryModel, self).__init__(parent)
        self.dataDictionary = {}
        self.cachedExpandedStates = {}

    def cacheExpandedStates(self):
        """
        Store the current expanded state for later.

        NOTE: This method gets called automatically when the setDictionary gets called.  This allows for resetting
              the view to the expanded state before the model was reset
        """
        self.cachedExpandedStates = self.getExpandedStates()

    def addItem(self, header=None, value=None, parentItem=None):
        """
        Add a key-value pair to the model.  This will create another row and add it to the model as a child to
        parentItem. If parentItem is None, it will be added as a top-level item in the model.

        #TODO: Figure out what to do when addItem is called with an existing parentItem.
        Until this is resolved, default all calls to add as top-level item
        - If dict, it should create a new key value pair
        - If list, it should add to end of list
        - If not either, what should happen?

        Args:
            header: name of the 'key' as a string
            value: value corresponding to this key
            parentItem: parent item in the model
        """
        header = self.make_unique_header(header)
        row = self.createRow(header, value)
        self.appendRow(row)
        self.refresh()

    def createRow(self, headerName=None, value=None):
        """
        Create a new row.

        This is an internal convenience function to create two new items for a row.

        Args:
            headerName: text to put in the header (the 'key' column)
            value: value

        Returns:
            List of QStandardItems

        """
        headerItem = DictionaryModelItem()
        valueItem = DictionaryModelItem()

        # We cannot set the text in the constructor if the value is not a string (like we have in a list)
        headerItem.setData(headerName, QtCore.Qt.DisplayRole)
        valueItem.setData(value, QtCore.Qt.DisplayRole)

        return (headerItem, valueItem)

    def data(self, index, role):
        """
        Override of data method.  Get's data out of the model

        Args:
            index: QIndex
            role:  QtCore.Qt.ItemDataRole

        Returns:
            Role value at the given index
        """
        if role == QtCore.Qt.EditRole:
            # For the EditRole, we cast the value as a string.  This prevents the delegate from doing strong
            # type checking.  Strong type checking disallows changing types on-the-fly, which is what is desired.
            data = super(DictionaryModel, self).data(index, role)
            return str(data)
        return super(DictionaryModel, self).data(index, role)

    def evaluate(self, data):
        try:
            d = ast.literal_eval(data)
        except SyntaxError as e:
            d = ast.literal_eval(data.__repr__())
        except ValueError as e:
            d = data
        return d

    def getCachedExpandedStates(self):
        """
        Retrieve the last cached expanded states

        Returns:
            Cached Expanded states as a recursive dictionary.  See getExpandedStates.
        """
        return self.cachedExpandedStates

    def getDictionary(self):
        """
        Get the data in the model as a Python dictionary

        Returns:
            Python dictionary
        """
        parent = self.invisibleRootItem()
        return self._getDictionary(parent)

    def getExpandedStates(self, parent=None):
        """
        Get the expanded state of items in the model.

        The expansion state is a recursive definition.  The top-level is a dictionary, where the key is the text value
        of the top-level column 0 entries.  The value is a tuple of expansion state of this level, and a dictionary of
        children's keys : tuples of expansion.

        Example:
        column 0 (name)
        ---------------
        + item_1           (expanded)
           - child_1_1     (not expanded)
           + child_1_2     (expanded)
             + child_1_2_1 (not expanded)
             + child_1_2_2 (not expanded)
        - item_2           (expanded)
           - child_2_1     (not expanded)

        Corresponding expanded state:
        {'item_1': (True, {'child_1_1' : (False, {}),
                           'child_1_2' : (True, {'child_1_2_1': (False, {}),
                                                 'child_1_2_2': (False, {})},
         'item_2': (True, {'child_2_1' : (False, {})}

            expansion state,

        Returns:
            Return a dictionary of keys and their current expanded state
        """
        if parent is None:
            parent = self.invisibleRootItem()

        states = {}
        for row in range(parent.rowCount()):
            item = parent.child(row, 0)
            idx = item.index()

            if item.rowCount() > 0:
                children = self.getExpandedStates(item)
            else:
                children = {}
            key = self.data(idx, QtCore.Qt.DisplayRole)

            expanded = self.data(idx, DictionaryModelItem.ExpandedRole)
            states[key] = (expanded, children)
        return states

    def getItemFromKeys(self, keys, parent=None):
        """
        Get a QStandardItem from a list of keys

        Args:
            keys: list of keys in the model
            parent: Parent item.

        Returns:
            QStandardItem corresponding to the value matched with the list of keys.
        """
        if parent is None:
            parent = self.invisibleRootItem()

        item = None
        for i in range(parent.rowCount()):
            keyItem   = parent.child(i, 0)
            valueItem = parent.child(i, 1)
            if str(keyItem.data(QtCore.Qt.DisplayRole)) == str(keys[0]):
                if len(keys) > 1:
                    return self.getItemFromKeys(keys[1:], keyItem)
                else:
                    item = valueItem
                    break
        return item

    def getValue(self, keys):
        """
        Get a value from a list of indexes.

        Example:
            data = {'a' : 1,
                    'b' : 2,
                    'c' : ['x','y','z']}


        getValue(['c',2]) will return 'z'

        Args:
            index: list of keys for the data

        Returns: value
        """
        val = self.getDictionary()
        for i in keys:
            val = val[i]
        return val

    def make_unique_header(self, header):
        """
        Creates a unique key when adding a new item to model
        
        Args:
            header (str): Desired key

        Returns:
            name: unique key
        """
        ind = 1
        name = header
        while True:
            if self.findItems(name):
                name = header + "<" + str(ind) + ">"
                ind += 1
            else:
                break
        return name

    def refresh(self):
        """
        Refresh the model
        """
        dictionary = self.getDictionary()
        self.setDictionary(dictionary)
        self.dictionaryRefreshed.emit()

    def removeItem(self, item):
        """
        Remove an item from the model

        Args:
            item: DictionaryItem that is in the model
        """
        parent = item.parent()
        if parent:
            self.removeRow(item.row(), parent.index())
        else:
            self.removeRow(item.row())
        self.refresh()

    def setDictionary(self, dictionary):
        """
        Set the dictionary.

        NOTE: This will completely reset the model.  Before resetting, the expanded states will be cached.

        Args:
            dictionary: python dictionary
        """
        self.cacheExpandedStates()
        self.beginResetModel()

        horizontalLabels = []
        for i in range(self.columnCount()):
            headerItem = self.horizontalHeaderItem(i)
            if headerItem is None:
                horizontalLabels.append('')
            else:
                horizontalLabels.append(str(headerItem.data(role=QtCore.Qt.DisplayRole)))
        self.clear()
        root = self.invisibleRootItem()
        for key, value in sorted(dictionary.items()):
            self._setDictionary(key, value, root)

        self.setHorizontalHeaderLabels(horizontalLabels)
        self.endResetModel()

    def setData(self, index, value, role=None):
        """

        Args:
            index: QModelIndex of the item to change
            value: value
            role: Role of the data to change as an int.  Usually use QtCore.Qt.<role_name>

        Returns:

        """
        old_value = self.evaluate(self.data(index, role))
        new_value = self.evaluate(value)
        rval = super(DictionaryModel, self).setData(index, value, role)
        if type(old_value) != type(new_value):
            self.refresh()
        return rval

    @QtCore.pyqtSlot(int)
    def setItemCollapsed(self, idx):
        """
        Set the item as being collapsed.

        This slot can be connected to the "collapsed" signal from a QTreeView

        Args:
            idx: Index of the model as a QModelIndex
        """
        self.setData(idx, False, DictionaryModelItem.ExpandedRole)

    def setItemExpanded(self, idx):
        """
        Set the item as being expanded

        This slot can be connected to the "expanded" signal from a QTreeView

        Args:
            idx: Index of the model as a QModelIndex
        """
        self.setData(idx, True, DictionaryModelItem.ExpandedRole)

    def setValue(self, keys, value):
        """
        Set a value in the dictionary

        Args:
            keys: list of indexes
            value: value to set

        Example:
            data = {'a' : 1,
                    'b' : 2,
                    'c' : ['x','y','z']}

        setValue(['c', 2], 4)

            data = {'a' : 1,
                    'b' : 2,
                    'c' : ['x','y',4]}
        """
        self.dataDictionary = self.getDictionary()
        res = self.dataDictionary
        for key in keys[:-1]:
            res = res.setdefault(key, {})
        res[keys[-1]] = value
        self.setDictionary(self.dataDictionary)

    def _getDictionary(self, parent):
        """
        Recursively build a dictionary from the Qt model

        Args:
            parent: parent item as a DictionaryModelItem

        Returns:
            Python dictionary
        """
        values = None
        if not parent.rowCount():
            return {}
        for row in range(parent.rowCount()):
            keyItem = parent.child(row, 0)
            key  = keyItem.data(QtCore.Qt.DisplayRole)
            data = parent.child(row, 1).data(QtCore.Qt.DisplayRole)
            raw_data = data

            # start of a new list / dictionary
            if keyItem.hasChildren() and data == None:
                data = self._getDictionary(keyItem)
            else:
                data = self.evaluate(data)

            if type(key) == int:
                if values is None:
                    values = []
                values.append(data)
            else:
                if values is None:
                    values = {}
                values[key] = data

        return values

    def _setDictionary(self, name, value, parent):
        """
        Recursively update the attributes of an object

        Args:
            name: Name of this object attribute
            value: Value of this attribute
            parent: The QtGui.QStandardItem containing the value for this attribute
        """
        keyItem, valueItem = self.createRow(headerName=name)
        parent.appendRow([keyItem, valueItem])

        if type(value) in atomic_types:
            valueItem.setData(value, QtCore.Qt.DisplayRole)

        elif type(value) in [list, tuple]:
            self._updateList(name, value, keyItem)

        elif type(value) is dict:
            self._updateDict(name, value, keyItem)

    def _updateList(self, name, obj, parent):
        """
        Update a list attribute

        Args:
            name: Name of this list attribute
            obj: Value of this attribute.  This should be a list.
            parent: The QtGui.QTreeWidgetItem containing the value for this attribute
        """
        for i in range(len(obj)):
            self._setDictionary(i, obj[i], parent)

    def _updateDict(self, name, obj, parent):
        """
        Update a dictionary attribute

        Args:
            name: Name of this dictionary attribute
            obj: Value of the dictionary.
            parent: The QtGui.QTreeWidgetItem containing the value for this attribute
        """
        for key, i in zip(sorted(obj), range(len(obj))):
            self._setDictionary(key, obj[key], parent)
