from pyqtlib import QtCore, QtGui

class AnyChildFilterProxyModel(QtGui.QSortFilterProxyModel):
    """
    This subclass of the QSortFilterProxyModel will match if any of it's children
    match the filterRegExp.

    This class uses filterRole() method from the baseclass to determine which role to use to filter
    data from the parent model.

    This class also provides an acceptRow() method to check if a row is accepted by passing in an index
    from the parent model.
    """

    def filterAcceptsRow(self, sourceRow, sourceParent):
        """
        Overload of the filterAcceptsRow method from the base class

        Returns true if this row or any children match
        """

        index = self.sourceModel().index(sourceRow,0,sourceParent)
        return self.acceptRow(index)

    def acceptRow(self, index):
        """
        @param index QModelIndex of the source model
        @return true if this row or any children match, otherwise false
        """
        if not index.isValid():
            return False

        regex = self.filterRegExp()
        data = index.data(self.filterRole())
        if regex.indexIn(data) >= 0:
            return True

        for row in range(self.sourceModel().rowCount(index)):
            if self.filterAcceptsRow(row, index):
                return True

        return False