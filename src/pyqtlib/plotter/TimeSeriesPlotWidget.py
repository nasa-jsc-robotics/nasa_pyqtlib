#!/usr/bin/env python
from pyqtlib import QtGui, QtCore
import numpy as np
import pyqtgraph
from collections import deque
import time
from datetime import datetime
from pyqtlib.table import SimpleTableWidget
import logging

MAX_COLOR_INDEX = 13
DEFAULT_PLOT_SPLIT = [800,150]

STAT_BUFFER_SIZE = 100
REFRESH_DELTA_T_BUFFER_SIZE = 100
PLOT_REFRESH_RATE  = 10 #Hz
DEFAULT_PLOT_WIDTH = 10 #seconds
DEFAULT_ZOOM_PLOT_WIDTH = DEFAULT_PLOT_WIDTH * 10 #seconds

logger = logging.getLogger(__name__)

def gettime():
    return time.time()


class TimeSeriesPlotItem(object):
    def __init__(self, name, parent_plot_items, update_callback=None, initial_values=None, max_size=None):
        """
        TimeSeriesPlotItem contains the data points for a PlotWidget plot.  The TimeSeriesPlotItem has two
        lists: 'data' and 'time', where the nth item of 'data' matches the nth item of 'time'
        @param name Name of this data item
        @param parent_plot_items The parent PlotItem from a PlotWidget
        @param update_callback Callback funcition used to update the data.  This callback must not
                              have any input arguments
        @param initial_values A numpy ndarray of shape N x 2, where the first column is time, and the second
                              column is data.  This will be the initial time and data values for this TimeSeriesPlotItem
        @param max_size The max number of data points contained in this TimeSeriesPlotItem
        """
        self._name = name
        self.updateCallback = update_callback
        self._plots = parent_plot_items
        self.max_size = max_size
        self.dataChanged = True

        self.setValues(initial_values)

    def setValues(self, values):
        '''
        Set the data values for this TimeSeriesPlotItem
        @param values A numpy.ndarry of values of dimension N x 2 where values[:,0] is time and values[:,1] is the data value
        @param resize_to_data When True, the data plotter buffers will reset to the same size as the data.
        '''
        if values is None:
            self.clear()
            return

        if values.ndim != 2:
            raise Exception("setValues argument value is of the wrong dimension. Should be 2, actually {}".format(values.ndim))

        if values.shape[1] != 2:
            raise Exception("setValues argument value is of the wrong shape. Should be n x 2, actually n x {}".format(values.shape[1]))

        self._time = deque(values[:,0],self.max_size)
        self._data = deque(values[:,1],self.max_size)

    def clear(self):
        self._data = deque([],self.max_size)
        self._time = deque([],self.max_size)

    @property
    def name(self):
        return self._name

    @property
    def data(self):
        return list(self._data)

    @property
    def time(self):
        return list(self._time)

    @property
    def plots(self):
        return self._plots

    def getValues(self):
        return (self.time, self.data)

    def getValueFromTime(self,t=None):
        if t is None:
            # use the latest data value
            try:
                data = self.data[-1]
                return self.data[-1]
            except IndexError:
                return None
        idx = np.abs(np.array(self.time) - t).argmin()
        return self.data[idx]

    def sampleData(self, t=None):
        if self.updateCallback != None:
            if t is None:
                t = time.time()
            self._data.append(self.updateCallback())
            self._time.append(t)
            self.dataChanged = True

    def appendValue(self,value):
        self._time.append(value[0])
        self._data.append(value[1])

    def appendData(self, data):
        self._data.append(data)
        self._time.append(gettime())

    def updatePlot(self):
        if self.dataChanged:
            for plot in self._plots:
                plot.setData(x=self.time, y=self.data)
                self.dataChanged = False

    def setPlotVisible(self, visible=True):
        for plot in self._plots:
            plot.setVisible(visible)

    def setMaxSize(self, size):
        self.max_size = size
        self.clear()

class TimeStampAxis(pyqtgraph.AxisItem):

    def __init__(self, *args, **kwargs):
        super(TimeStampAxis, self).__init__(*args,**kwargs)
        self.timeStampMode = True

    def tickStrings(self, values, scale, spacing):
        strns = []
        if self.timeStampMode:
            timeString = '%Y-%m-%d %H:%M:%S'
            for x in values:
                strns.append(time.strftime(timeString,time.localtime(x)))
            return strns
        else:
            for x in values:
                strns.append('{:.6f}'.format(x))
        return strns

    def setTimeStampMode(self, timeStampOn):
        self.timeStampMode = timeStampOn


class TimeSeriesPlotWidget(QtGui.QWidget):
    def __init__(self,parent=None):
        super(TimeSeriesPlotWidget, self).__init__(parent)

        # Create a custom time axis to display a time string
        self.mainPlotTimeAxis = TimeStampAxis(orientation='bottom')
        self.zoomPlotTimeAxis = TimeStampAxis(orientation='bottom')

        # Create the main plot, the zoom plot, and the zoom region box
        self.plotWidget = pyqtgraph.PlotWidget()
        self.plotWidget = pyqtgraph.PlotWidget(axisItems={'bottom':self.mainPlotTimeAxis})
        self.zoomPlotWidget = pyqtgraph.PlotWidget(axisItems={'bottom':self.zoomPlotTimeAxis})
        self.zoomRegionItem = pyqtgraph.LinearRegionItem()
        self.zoomPlotWidget.addItem(self.zoomRegionItem)

        # Create the simple table widget
        self.table = SimpleTableWidget()

        # Create handles to the PlotWidget's members
        self.viewBox = self.plotWidget.getPlotItem().vb

        # setup the main plot's options
        self.plotWidget.getPlotItem().showGrid(x=True,y=True)
        self.viewBox.enableAutoRange(axis=pyqtgraph.ViewBox.YAxis)

        # Create the Legend for the graph
        self.legend = pyqtgraph.LegendItem(size=None, offset=(60, 15))
        self.legend.setParentItem(self.plotWidget.getPlotItem())

        #cross hairs
        self.vLine = pyqtgraph.InfiniteLine(angle=90, movable=False)
        self.hLine = pyqtgraph.InfiniteLine(angle=0, movable=False)
        self.plotWidget.addItem(self.vLine)
        self.plotWidget.addItem(self.hLine)
        self.crosshairTextItem = pyqtgraph.TextItem()
        self.viewBox.addItem(self.crosshairTextItem)

        # Parameters
        self.max_data_item_size = None
        self.max_color_index = 13

        # empty dictionary containing all the data plotter items
        self.plotterItems = {}

        # name : bool
        self.plotNamesVisible = {}

        # name : PlotItem
        self.legendPlotDict = {}

        self.plotUpdateDeltaTimes = deque(maxlen=STAT_BUFFER_SIZE)
        self.plotUpdateDurations = deque(maxlen=STAT_BUFFER_SIZE)
        self.lastPlotUpdateTime = time.time()

        #auto scroll features
        self.autoScrollCheckbox = QtGui.QCheckBox(text='&Autoscroll')
        self.autoScrollCheckbox.setCheckState(QtCore.Qt.Unchecked)
        self.autoScrollCheckbox.stateChanged.connect(self.autoScrollClicked)
        self.autoScroll = False

        # cross hairs enable checkbox
        self.crosshairsCheckbox = QtGui.QCheckBox(text='&Crosshairs')
        self.crosshairsCheckbox.setCheckState(QtCore.Qt.Unchecked)
        self.crosshairsCheckbox.stateChanged.connect(self.crosshairsClicked)
        self.crosshairsEnabled = False
        self.showCrosshairs(False)

        #view legend checkbox
        self.legendCheckbox = QtGui.QCheckBox(text='&Legend')
        self.legendCheckbox.setCheckState(QtCore.Qt.Unchecked)
        self.legendCheckbox.stateChanged.connect(self.setLegendVisible)
        self.setLegendVisible(False)

        #show table checkbox
        self.tableCheckbox = QtGui.QCheckBox(text='&Table')
        self.tableCheckbox.setCheckState(QtCore.Qt.Checked)
        self.tableCheckbox.stateChanged.connect(self.setTableVisible)

        #show Zoom Plot
        self.zoomPlotCheckbox = QtGui.QCheckBox(text='&ZoomPlot')
        self.zoomPlotCheckbox.setCheckState(QtCore.Qt.Checked)
        self.zoomPlotCheckbox.stateChanged.connect(self.setZoomPlotVisible)

        #time axis
        self.timeFormatCheckbox = QtGui.QCheckBox(text='&TimeFormat')
        self.timeFormatCheckbox.setCheckState(QtCore.Qt.Checked)
        self.timeFormatCheckbox.stateChanged.connect(self.mainPlotTimeAxis.setTimeStampMode)
        self.timeFormatCheckbox.stateChanged.connect(self.zoomPlotTimeAxis.setTimeStampMode)

        # fps label
        self.avgFpsLabel = QtGui.QLabel()
        self.avgFpsLabel.setFixedWidth(80)
        self.avgFps = 0
        self.lastFpsUpdate = time.time()

        # sps label
        self.avgSpsLabel = QtGui.QLabel()
        self.avgSpsLabel.setFixedWidth(80)
        self.avgSps = 0
        self.lastSpsUpdate = time.time()


        # set the layouts for the widget
        self.plotSplitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        self.plotSplitter.addWidget(self.plotWidget)
        self.plotSplitter.addWidget(self.zoomPlotWidget)
        self.plotSplitter.setSizes(DEFAULT_PLOT_SPLIT)

        self.checkBoxLayout = QtGui.QHBoxLayout()
        self.checkBoxLayout.addWidget(self.autoScrollCheckbox)
        self.checkBoxLayout.addWidget(self.crosshairsCheckbox)
        self.checkBoxLayout.addWidget(self.legendCheckbox)
        self.checkBoxLayout.addWidget(self.tableCheckbox)
        self.checkBoxLayout.addWidget(self.zoomPlotCheckbox)
        self.checkBoxLayout.addWidget(self.timeFormatCheckbox)
        self.checkBoxLayout.addWidget(self.avgFpsLabel)
        self.checkBoxLayout.addWidget(self.avgSpsLabel)

        self.plotsLayout = QtGui.QVBoxLayout()
        self.plotsLayout.addWidget(self.plotSplitter)
        self.plotsLayout.addLayout(self.checkBoxLayout)

        plots = QtGui.QWidget()
        plots.setLayout(self.plotsLayout)

        self.plot_table_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        self.plot_table_splitter.addWidget(plots)
        self.plot_table_splitter.addWidget(self.table)

        self.layout = QtGui.QHBoxLayout()
        self.layout.addWidget(self.plot_table_splitter)
        self.setLayout(self.layout)

        self.plotWidget.sigRangeChanged.connect(self.updateZoomRegionFromView)
        self.zoomRegionItem.sigRegionChanged.connect(self.updateViewFromZoom)

        self.resetPlotToDefaults()

        self.plotRefreshTimer = QtCore.QTimer()
        self.plotRefreshTimer.timeout.connect(self.updateView)
        self.setRefreshRate(PLOT_REFRESH_RATE)

        self.show()

    def updateView(self):
        self.updatePlot()
        self.updateCrossHairs()

        if self.autoScroll == True:
            zoomStart,zoomEnd = self.zoomRegionItem.getRegion()
            zoomWidth = zoomEnd - zoomStart
            maxTime = self.getMaxTime()
            if maxTime is None:
                maxTime = time.time()

            self.zoomRegionItem.setRegion([maxTime - zoomWidth, maxTime])
            self.zoomPlotWidget.setXRange(maxTime - self.getCurrentViewTimeWidth(self.zoomPlotWidget), maxTime, padding=0)

    def updateCrossHairs(self):
        if not self.crosshairsEnabled:
            # show latest value in table
            if self.table.isVisible():
                self.updateTableValues(None)
            return
        # Grab the position of the mouse
        pos = QtGui.QCursor.pos()
        #Translate from screen coordinates to scene coordinates
        relative_pos = QtCore.QPointF(self.plotWidget.mapFromGlobal(pos))
        #Check if the position is in the scene
        if self.plotWidget.sceneBoundingRect().contains(relative_pos):
            # Map the coordinates from scene coordinates to the view coordinates
            mousePoint = self.viewBox.mapSceneToView(relative_pos)
            timeValue  = mousePoint.x()
            # Set the text of the crosshairs
            if self.timeFormatCheckbox.isChecked():
                timeFormat = '%H:%M:%S'
                dt = datetime.fromtimestamp(timeValue)
                timeString = '{:02}:{:02}:{:02}.{:02}'.format(dt.hour,dt.minute,dt.second,dt.microsecond)
            else:
                timeString = '{:6f}'.format(timeValue)

            self.crosshairTextItem.setText('x={} y={}'.format(timeString,mousePoint.y()))

            # set the position of the crosshairs and the text
            self.vLine.setVisible(True)
            self.hLine.setVisible(True)
            self.vLine.setPos(mousePoint.x())
            self.hLine.setPos(mousePoint.y())
            self.crosshairTextItem.setPos(mousePoint.x(),mousePoint.y())
            self.crosshairTextItem.setVisible(True)

            # make sure cross hairs and text are on top
            self.crosshairTextItem.setZValue(10)
            self.vLine.setZValue(10)
            self.hLine.setZValue(10)

            self.updateTableValues(mousePoint.x())

            #self.printCrosshairValues(mousePoint.x())
        else:
            # show latest value in table
            self.updateTableValues(None)
            self.vLine.setVisible(False)
            self.hLine.setVisible(False)
            self.crosshairTextItem.setVisible(False)

    def updateData(self):
        for plotterItem in self.plotterItems.values():
            plotterItem.update()

    def updatePlot(self):
        self.updateAvgFps()
        t = time.time()
        self.plotUpdateDeltaTimes.append(t - self.lastPlotUpdateTime)
        self.lastPlotUpdateTime = t
        for plotterItem in self.plotterItems.values():
            plotterItem.updatePlot()
        delta_t = time.time() - t
        self.plotUpdateDurations.append(delta_t)
        self.avgSpsLabel.setText('sps:{:>4.0f}'.format(self.avgSps))

    def updateAvgFps(self):
        now = time.time()
        fps = 1.0 / (now - self.lastFpsUpdate)
        self.avgFps = self.avgFps * 0.8 + fps * 0.2
        self.lastFpsUpdate = now
        self.avgFpsLabel.setText('fps:{:>6.2f}'.format(self.avgFps))

    def sampleData(self, t=None):
        self.updateAvgSps()
        for plotterItem in self.plotterItems.values():
            plotterItem.sampleData(t)

    def updateAvgSps(self):
        now = time.time()
        sps = 1.0 / (now - self.lastSpsUpdate)
        self.avgSps = self.avgSps * 0.8 + sps * 0.2
        self.lastSpsUpdate = now

    def updateTableValues(self, t):
        values = self.getTimeValues(t)
        for name, value in values.items():
            self.table.setValue(name, value)

    def getPlotUpdateRateStat(self):
        try:
            return 1.0 / np.mean(self.plotUpdateDeltaTimes)
        except ZeroDivisionError:
            return 0

    def getPlotUpdateDurationStat(self):
        return np.mean(self.plotUpdateDurations)

    def getTimeValues(self, t):
        valueDict = {}
        for name,plotterItem in sorted(self.plotterItems.items()):
            valueDict[name] = plotterItem.getValueFromTime(t)
        return valueDict

    def addPlot(self, name, update_callback=None, initial_values=None):
        mainPlot = self.plotWidget.plot(name=name, pen=(len(self.plotterItems),self.max_color_index), clipToView=True)

        zoomPlot = self.zoomPlotWidget.plot(name=name, pen=(len(self.plotterItems),self.max_color_index), clipToView=True)
        plotterItem = TimeSeriesPlotItem(name, [mainPlot, zoomPlot], update_callback, initial_values, self.max_data_item_size)
        self.plotterItems[name] = plotterItem
        self.plotNamesVisible[name] = True
        self.legend.setZValue(10)
        self.table.addValue(name)
        self.updateLegend()

    #############
    # Legend Helper Methods
    #############

    def addLegendItem(self, name, parentPlotItem):
        self.legendPlotDict[name] = parentPlotItem
        self.legend.addItem(parentPlotItem, name)

    def removeLegendItem(self, name):
        if name in self.legendPlotDict.keys():
            del self.legendPlotDict[name]
            self.legend.removeItem(name)

    def removeAllLegendItems(self):
        for name in self.legendPlotDict.keys():
            self.removeLegendItem(name)

    def updateLegend(self):
        if self.legend.isVisible():
            self.removeAllLegendItems()
            for name in sorted(self.plotterItems.keys()):
                if self.plotNamesVisible[name]:
                    self.addLegendItem(name, self.plotterItems[name].plots[0])

    def setLegendVisible(self, visible):
        if visible and not self.legend.isVisible():
            self.legendCheckbox.setCheckState(QtCore.Qt.Checked)
            self.legend.setVisible(visible)
            self.updateLegend()
        else:
            self.legend.setVisible(visible)
            self.legendCheckbox.setCheckState(QtCore.Qt.Unchecked)

    def setZoomPlotVisible(self, visible):
        if visible:
            self.zoomPlotCheckbox.setCheckState(QtCore.Qt.Checked)
        else:
            self.zoomPlotCheckbox.setCheckState(QtCore.Qt.Unchecked)
        self.zoomPlotWidget.setVisible(visible)

    def setTableVisible(self, visible):
        if visible:
            self.tableCheckbox.setCheckState(QtCore.Qt.Checked)
        else:
            self.tableCheckbox.setCheckState(QtCore.Qt.Unchecked)
        self.table.setVisible(visible)

    def removePlot(self,name):
        if name in self.plotterItems.keys():
            self.table.removeValue(name)
            self.plotWidget.removeItem(self.plotterItems[name].plots[0])
            self.zoomPlotWidget.removeItem(self.plotterItems[name].plots[1])
            del self.plotterItems[name]
            del self.plotNamesVisible[name]
            self.updateLegend()

    def removeAllPlots(self):
        for plotName in self.plotterItems.keys():
            self.removePlot(plotName)

    def appendValue(self,name, value):
        self.plotterItems[name].appendValue(value)

    def setValues(self, name, values):
        self.plotterItems[name].setValues(values)
        minTime, maxTime = self.getDataTimeRange()
        self.zoomPlotWidget.setXRange(minTime, maxTime,padding=0)
        self.zoomRegionItem.setRegion([minTime, maxTime])

    def getCurrentViewTimeWindow(self, plotWidget):
        return plotWidget.getPlotItem().getViewBox().viewRange()[0]

    def getCurrentViewTimeWidth(self, plotWidget):
        startTime, stopTime = self.getCurrentViewTimeWindow(plotWidget)
        return stopTime - startTime

    def getMinTime(self):
        minimum = None
        for plotterDataItem in self.plotterItems.values():
            time = plotterDataItem.time
            if len(time) > 0:
                timeMin = min(time)
                if minimum == None:
                    minimum = timeMin
                elif minimum > timeMin:
                    minimum = timeMin
        return minimum

    def getMaxTime(self):
        maximum = None
        for plotterDataItem in self.plotterItems.values():
            time = plotterDataItem.time
            if len(time) > 0:
                timeMax = max(time)
                if maximum == None:
                    maximum = timeMax
                elif maximum > timeMax:
                    maximum = timeMax
        return maximum

    def getDataTimeRange(self):
        minTime = self.getMinTime()
        maxTime = self.getMaxTime()
        if minTime == None:
            minTime = time.time()
        if maxTime == None:
            maxTime = time.time()
        return minTime,maxTime

    def updateViewFromZoom(self):
        minX, maxX = self.zoomRegionItem.getRegion()
        self.plotWidget.setXRange(minX, maxX, padding=0)

    def updateZoomRegionFromView(self, window, viewRange):
        if type(viewRange) is not list:
            return
        self.zoomRegionItem.setZValue(10)
        self.zoomRegionItem.setRegion(viewRange[0])

    def autoScrollClicked(self,checkState):
        if checkState == QtCore.Qt.Checked:
            self.autoScroll = True

        elif checkState == QtCore.Qt.Unchecked:
            self.autoScroll = False

    def showCrosshairs(self, show=True):
        self.vLine.setVisible(show)
        self.hLine.setVisible(show)
        self.crosshairTextItem.setVisible(show)

    def crosshairsClicked(self,checkState):
        self.crosshairsEnabled = (checkState == QtCore.Qt.Checked)
        self.showCrosshairs(self.crosshairsEnabled)

    def setPlotVisible(self, name, visible=True):
        if name in self.plotterItems.keys():
            self.plotterItems[name].setPlotVisible(visible)
            self.plotNamesVisible[name] = visible
            self.table.setValueVisible(name,visible)
            self.updateLegend()

    def setRefreshRate(self, rate):
        """
        Set the refresh rate of the plot areas

        :type rate: float
        :param rate: rate in Hz.  If set to zero, the refresh rate will be as fast as possible
        """
        try:
            refreshSleep = 1.00/rate *1000.00
        except ZeroDivisionError:
            refreshSleep = 0.0
        self.plotRefreshRate = rate
        self.plotRefreshTimer.start(refreshSleep)

    def setMaxDataSize(self, size):
        self.max_data_item_size = size
        for plotterItem in self.plotterItems.values():
            plotterItem.setMaxSize(self.max_data_item_size)

    def getMaxDataSize(self):
        return self.max_data_item_size

    def getRefreshRate(self):
        return self.plotRefreshRate

    def setAutoScroll(self, autoScrollOn):
        if autoScrollOn:
            self.autoScrollCheckbox.setCheckState(QtCore.Qt.Checked)
            self.autoScroll = True
        else:
            self.autoScrollCheckbox.setCheckState(QtCore.Qt.Unchecked)
            self.autoScroll = False

    def setPlotTimeWindowWidth(self, width):
        maxTime = self.getMaxTime()
        if maxTime is None:
            maxTime = time.time()
        self.plotWidget.setXRange(maxTime-width, maxTime, padding=0)

    def setZoomTimeWindowWidth(self, width):
        maxTime = self.getMaxTime()
        if maxTime is None:
            maxTime = time.time()
        self.zoomPlotWidget.setXRange(maxTime-width, maxTime, padding=0)

    def enableAutoScale(self):
        """
        Enable auto range on the Y axis
        """
        self.viewBox.enableAutoRange(axis=pyqtgraph.ViewBox.YAxis)

    def resetPlotToDefaults(self):
        self.setZoomTimeWindowWidth(DEFAULT_ZOOM_PLOT_WIDTH)
        self.setPlotTimeWindowWidth(DEFAULT_PLOT_WIDTH)
        self.enableAutoScale()
