from pyqtlib import QtGui, QtCore

class InputWidgetInterface(QtGui.QWidget):
    def __init__(self, parent=None):
        super(InputWidgetInterface, self).__init__(parent)

    def getValue(self):
        raise NotImplementedError("This method has not be implemented")

    def setValue(self, value):
        raise NotImplementedError("This method has not be implemented")

    def getWidget(self):
        raise NotImplementedError("This method has not be implemented")


class LineEdit(QtGui.QLineEdit):
    """
    Overloaded QLineEdit so that we can implement a minimum size that should show all the text.
    """

    def minimumSizeHint(self):
        fm = self.fontMetrics()
        rect = fm.boundingRect(self.text())
        return QtCore.QSize(rect.width(),rect.height())

class LineEditInputWidget(InputWidgetInterface):

    def __init__(self, dataType=str, lineEdit=None):
        super(LineEditInputWidget, self).__init__()
        if not lineEdit:
            lineEdit = LineEdit()
        self.widget = lineEdit
        self.dataType = dataType
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.widget)
        self.setLayout(layout)

    def getValue(self):
        return self.widget.text()

    def getWidget(self):
        return self.widget

    def setValue(self, value):
        self.widget.setText(value)

class ComboBoxInputWidget(InputWidgetInterface):

    def __init__(self, dataType=str, comboBox=None):
        super(ComboBoxInputWidget, self).__init__()
        if not comboBox:
            self.widget = QtGui.QComboBox()
        self.widget = comboBox
        self.dataType = dataType
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.widget)
        self.setLayout(layout)

    def getValue(self):
        return self.widget.currentText()

    def setValue(self,value):
        index = self.widget.findText(value)
        if index >= 0:
            self.widget.setCurrentIndex(index)

    def getWidget(self):
        return self.widget

class DictionaryDialog(QtGui.QDialog):
    def __init__(self, title=None, parent=None):
        super(DictionaryDialog, self).__init__(parent=parent)
        self.formLayout = QtGui.QFormLayout()
        self.editWidgets = {}

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        if title:
            self.setWindowTitle(title)

        layout = QtGui.QVBoxLayout()
        layout.addLayout(self.formLayout)
        layout.addWidget(buttonBox)
        self.setLayout(layout)
        self.show()

    def addSetting(self, name, inputWidget=None, default_value=None, dataType=str):
        """
        Add a settings widget.

        By default, this will add a LineEditInputWidget, which is a wrapper around a QLineEdit

        :param name: name and label for the particular setting
        :param inputWidget: Instance of an input widget to add to the form.  This must be a subclass of the InputWidgetInterface
        :param default_value: Default value for the widget
        :param dataType: Data type for the widget.  The return value will be cast to this type
        """
        if inputWidget is None:
            inputWidget = LineEditInputWidget(dataType)

        # Set the initial value
        if default_value:
            inputWidget.setValue(default_value)

        self.editWidgets[name] = inputWidget
        self.formLayout.addRow(name, inputWidget.getWidget())

    def getData(self):
        """
        Get the data from all input widgets as a dictionary
        :return: dict of values
        """
        data = {}
        for name, inputWidget in self.editWidgets.items():
            data[name] = inputWidget.getValue()
        return data