from pyqtlib import QtGui, QtCore

class CallbackMapper(object):
    """
    Maps an input function to an output function.

    """
    def __init__(self, sourceMethod, targetMethod, cast=None):
        """
        @type sourceMethod Python callable
        @param sourceMethod The source method that will be invoked.  This method will not be sent any arguments

        @type targetMethod Python callable
        @param targetMethod This method will be called using the sourceMethod's output as it's input

        @type cast Python type
        @param cast Optional cast for the value returned from the sourceMethod
        """
        self.cast = cast
        self.source = sourceMethod
        self.target = targetMethod

    def __call__(self):
        value = self.source()
        if self.cast:
            value = self.cast(value)
        self.target(value)


class LineEditFormWidget(QtGui.QDialog):
    """
    This is a convenience widget for auto-creating a QFormLayout composed of QLineEdits.

    After instantiating, calls to "addSetting" will populate the widget with a label and a QLineEdit.
    When "enter/return" is pressed when the QLineEdit is in focus, the attached slot will be called with
    the new value.
    """
    def __init__(self, parent=None):
        super(LineEditFormWidget,self).__init__(parent)
        self.formLayout = QtGui.QFormLayout()
        self.editWidgets = {}
        self.currentSettings = {}
        self.defaultSettings = {}
        self.callbackMappers = {}

        self.applyButton = QtGui.QPushButton('&Apply')
        self.defaultsButton = QtGui.QPushButton('&Defaults')
        self.cancelButton = QtGui.QPushButton('&Cancel')

        self.applyButton.clicked.connect(self.applySettings)
        self.defaultsButton.clicked.connect(self.setDefaults)
        self.cancelButton.clicked.connect(self.close)

        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addWidget(self.applyButton)
        buttonLayout.addWidget(self.defaultsButton)
        buttonLayout.addWidget(self.cancelButton)

        layout = QtGui.QVBoxLayout()
        layout.addLayout(self.formLayout)
        layout.addLayout(buttonLayout)

        self.setLayout(layout)

        # disable the apply button until settings have been changed
        self.disableApplyButton()

    @QtCore.pyqtSlot()
    def applySettings(self):
        """
        Apply the current settings.  This slot is connected to the "Apply" button
        """
        for label, callbackMapper in self.callbackMappers.items():
            if self.currentSettings[label] != self.editWidgets[label].text():
                callbackMapper()
        self.storeCurrentSettings()
        self.disableApplyButton()

    def addSetting(self, label, slot, default, slotType=None, validator=None):
        """
        Add a setting to the dialog

        :param label: Label for the setting
        :param slot: Slot that will be called when this setting is changed
        :param default: The starting value for the setting
        :param slotType: If set, the text from the text edit will be cast to this type
        :param validator: A QValidator that can be applied to the line edit

        :return: The QLineEdit that was created
        """

        # Create the setting editor
        lineEdit = QtGui.QLineEdit()
        if validator:
            lineEdit.setValidator(validator)

        # Set the initial value
        lineEdit.setText(str(default))

        # Whenever we edit a setting, enable the "Apply" button
        lineEdit.textChanged.connect(self.enableApplyButton)

        # Create a CallbackMapper.  The CallbackMapper will call the slot when "Apply" is pressed.
        callbackMapper = CallbackMapper(lineEdit.text,slot,slotType)
        self.callbackMappers[label] = callbackMapper

        # Store out edit widget and settings
        self.editWidgets[label] = lineEdit
        self.currentSettings[label] = default
        self.defaultSettings[label] = default

        # Add setting to the layout
        self.formLayout.addRow(label,lineEdit)

        return lineEdit

    @QtCore.pyqtSlot(object)
    def closeEvent(self, event):
        """
        Overload the closeEvent to reset uncommited settings
        :param event:Close Event
        :return:
        """
        self.restoreCurrentSettings()
        self.disableApplyButton()
        return super(LineEditFormWidget,self).closeEvent(event)

    def disableApplyButton(self):
        """
        Disable the "Apply" button
        """
        self.applyButton.setEnabled(False)

    @QtCore.pyqtSlot()
    def enableApplyButton(self):
        """
        Enable the "Apply" button.  This slot is connected to every setting.
        """
        self.applyButton.setEnabled(True)

    def restoreCurrentSettings(self):
        """
        Restore all settings to the last applied value.  This gets called when the "Cancel" button is checked.
        """
        for label, setting in self.currentSettings.items():
            self.editWidgets[label].setText(str(setting))

    @QtCore.pyqtSlot()
    def setDefaults(self):
        """
        Set all edit widgets to the default values.  User still must commit by clicking "Apply".
        This slot is connected to the "Defaults" button
        """
        for label, widget in self.editWidgets.items():
            self.editWidgets[label].setText(str(self.defaultSettings[label]))

    def storeCurrentSettings(self):
        """
        Store the current settings.  Used for when we need to cancel
        """
        for label, widget in self.editWidgets.items():
            self.currentSettings[label] = widget.text()


