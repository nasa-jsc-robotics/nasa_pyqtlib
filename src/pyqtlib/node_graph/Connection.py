from pyqtlib import QtGui, QtCore
from .Element import Element

class Connection(Element):

    def __init__(self, parent=None):
        super(Connection, self).__init__(parent=parent)

        self.moveable = False
        self.setPen(QtGui.QPen(QtCore.Qt.black,2))

        self._startItem = None
        self._endItem   = None

        self._startPoint  = QtCore.QPointF(0,0)
        self._endPoint    = QtCore.QPointF(0,0)


    def connect(self, startItem, endItem):
        self.startItem = startItem
        self.endItem = endItem
        self.startItem.connect(self)
        self.endItem.connect(self)
        self.setZValue(min(self.startItem.zValue(), self.endItem.zValue()) - 1)

    def disconnect(self):
        if self.startItem is not None:
            self.startItem.disconnect(self)

        if self.endItem is not None:
            self.endItem.disconnect(self)

        self.startItem = None
        self.endItem = None

        # if self.scene() is not None:
        #     self.scene().removeItem(self)

    # def itemChange(self, change, value):
    #     """
    #
    #     Args:
    #         change : QGraphicsItem_GraphicsItemChange
    #         value : QVariant
    #
    #     Returns:
    #
    #     """
    #     if change == QtGui.QGraphicsItem.ItemSceneHasChanged:
    #         if value == None :
    #             self.disconnect()
    #     return super(Connection, self).itemChange(change, value)

    @property
    def endItem(self):
        return self._endItem

    @endItem.setter
    def endItem(self, value):
        self._endItem = value
        self.update()

    @property
    def endPoint(self):
        if self.endItem is not None:
            return self.endItem.connectPointInScene()
        else:
            return self._endPoint

    @endPoint.setter
    def endPoint(self, value):
        self._endPoint = value
        self.update()

    def isConnectedToItem(self, item):
        """
        Returns: Whether or not an item is attached to this connector
        """
        if self.endItem == item or self.startItem == item:
            return True
        else:
            return False

    def shape(self):
        """
        By overloading the "shape" method, we can make it easier to select the line
        Returns:
            QPainterPath
        """
        shape = QtGui.QGraphicsPathItem.shape(self)
        rect = shape.boundingRect().adjusted(-5., -5., 5., 5.)
        path = QtGui.QPainterPath()
        path.addRect(rect)
        return path

    @property
    def startItem(self):
        return self._startItem

    @startItem.setter
    def startItem(self, value):
        self._startItem = value
        self.update()

    @property
    def startPoint(self):
        if self.startItem is not None:
            return self.startItem.connectPointInScene()
        else:
            return self._startPoint

    @startPoint.setter
    def startPoint(self, value):
        self._startPoint = value
        self.update()

    def continueEvent(self, event):
        """
        Event received when the mouse is pressed again but not on another connectable item.

        This is meant to be overloaded in the subclass.

        Args:
            event: Mouse press event

        Returns:

        """
        return True


class LineConnection(Connection):

    def paint(self, painter, option, widget):
        #print 'start: {},{}  end: {},{}'.format(startPoint.x(),startPoint.y(), endPoint.x(),endPoint.y())
        startPoint = self.startPoint
        endPoint   = self.endPoint
        self.path = QtGui.QPainterPath(startPoint)
        self.path.lineTo(endPoint)
        self.setPath(self.path)
        #self.resetTextPosition()
        super(LineConnection, self).paint(painter, option, widget)

class SplineConnection(Connection):

    def paint(self, painter, option, widget):
        startPoint = self.startPoint()
        endPoint = self.endPoint()
        x1 = startPoint.x()
        y1 = startPoint.y()
        x2 = endPoint.x()
        y2 = endPoint.y()

        fx = 1
        fy = 0
        dx = abs(x2 - x1)*fx
        dy = abs(y2 - y1)*fy
        cx1 = x1 + dx
        cy1 = y1 + dy
        cx2 = x2 - dx
        cy2 = y2 - dy
        self.path = QtGui.QPainterPath(startPoint)
        self.path.cubicTo(cx1, cy1,
                     cx2, cy2,
                     x2,  y2)
        self.setPath(self.path)
        super(SplineConnection, self).paint(painter, option, widget)

