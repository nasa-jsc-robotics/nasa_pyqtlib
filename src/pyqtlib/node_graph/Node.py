from pyqtlib import QtGui, QtCore

from .Element import Element
from .Port import PortOrientation, Port
from .TextItem import TextItem

class Node(Element):

    DefaultColor = QtGui.QColor(QtCore.Qt.yellow)
    LighterAmount = 100
    InstanceNameFontSize = 12
    TypeNameFontSize = 8

    def __init__(self, instanceName='', typeName='', parent=None):
        Element.__init__(self, instanceName, parent)

        self._portMap = {}
        self.ports = []

        self.instanceFont = QtGui.QFont()
        self.instanceFont.setPointSize(Node.InstanceNameFontSize)
        self.instanceFont.setBold(True)

        self.typeFont = QtGui.QFont()
        self.typeFont.setPointSize(Node.TypeNameFontSize)
        self.typeFont.setItalic(True)

        self.instanceTextItem = TextItem(instanceName, parent=self)
        self.instanceTextItem.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)
        self.typeTextItem = TextItem(typeName, parent=self)
        self.typeTextItem.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)
        self.typeTextItem.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        self.typeTextItem.setFlag(QtGui.QGraphicsItem.ItemIsFocusable, False)

        self.instanceTextItem.setFont(self.instanceFont)
        self.typeTextItem.setFont(self.typeFont)

        self._color = None
        self.color = Node.DefaultColor

    def addPort(self, port):
        port.moveable = False
        port.selectable = False
        self.ports.append(port)
        self._portMap[port.name] = port
        port.setParentItem(self)
        self.updatePath()

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        """
        Set the color of the object.

        Args:
            value: Color Value defined in the QtCore.Qt.Global Color enum
        """
        self._color = value
        self.setBrush(self._color)

    def contextMenuEvent(self, event):
        menu = self.getContextMenu(event)
        if menu is not None:
            menu.exec_(event.screenPos())

    def deserialize(self, definition):
        ports = definition['ports']
        for idx in range(len(ports)):
            port = self.getPort(ports[idx]['name'])
            port.orientation = ports[idx]['orientation']
            while (idx != self.ports.index(port)):
                if self.ports.index(port) > idx:
                    self.movePortUp(port)
                else:
                    self.movePortDown(port)
        self.setPos(*definition['pos'])

    def getContextMenu(self, event):
        """
        Create and return the context menu for this item.

        Args:
            event: QGraphicsSceneContextMenuEvent

        Returns:
            QtGui.QMenu or None
        """

        port = self.getPortFromPos(event.scenePos())
        if port is not None:
            menu = port.getContextMenu(event)
            movePortUpAction   = menu.addAction('Move port up')
            movePortUpAction.triggered.connect(lambda : self.movePortUp(port))

            movePortDownAction = menu.addAction('Move port down')
            movePortDownAction.triggered.connect(lambda : self.movePortDown(port))

            return menu

        return None

    def getPort(self, name):
        """
        Get a Port instance by name

        Args:
            name: name of the port as a string

        Returns:
            Port object
        """
        return self._portMap[name]

    def getPortNames(self):
        """
        Get a list of the port names

        Returns:
            List of strings
        """
        return self._portMap.keys()

    def getPortFromPos(self, scenePos):
        """
        Get the port which contains a point.

        Args:
            scenePos: Position in the scene

        Returns:
            Port object, or None
        """
        for port in self.ports:
            localPos = port.mapFromScene(scenePos)
            if port.boundingRect().contains(localPos):
                return port
        return None

    def getPortFromItem(self, item):
        """
        Get the Port object which is an ancestor of an item

        Args:
            item: item which is a descendent of a port

        Returns:
            Port object or None
        """
        for port in self._portMap.values():
            if port.isAncestorOf(item):
                return port
        return None

    @property
    def instanceName(self):
        return self.instanceTextItem.text()

    def itemChange(self, change, value):
        """
        Gets called when an item changes
        Args:
            change : QGraphicsItem.GraphicsItemChange
            value : QVariant

        Returns:

        """
        if change == QtGui.QGraphicsItem.ItemSceneChange:
            if value is None:
                for port in self.ports:
                    port.disconnectAll()
        if change == QtGui.QGraphicsItem.ItemChildRemovedChange:
            if isinstance(value, Port):
                self.removePort(value.name)

        return super(Node, self).itemChange(change, value)

    def movePortDown(self, port):
        if port in self.ports:
            index = self.ports.index(port)
            if index < len(self.ports)-1:
                p = self.ports.pop(index)
                self.ports.insert(index + 1, p)
                self.updatePath()

    def movePortUp(self, port):
        if port in self.ports:
            index = self.ports.index(port)
            if index > 0:
                p = self.ports.pop(index)
                self.ports.insert(index-1, p)
                self.updatePath()

    def moveSelectedPortDown(self):
        items = self.scene().selectedItems()
        if len(items) > 0:
            item = items[0]
            if isinstance(item, Port):
                self.movePortDown(item)

    def moveSelectedPortUp(self):
        items = self.scene().selectedItems()
        if len(items) > 0:
            item = items[0]
            if isinstance(item, Port):
                self.movePortUp(item)

    def removePort(self, name):
        if name in self._portMap.keys():
            port = self._portMap.pop(name)
            self.ports.remove(port)
            port.disconnectAll()
            port.setParentItem(None)
            self.updatePath()

    def sceneEventFilter(self, item, event):
        """
        Filter child events.  This overload is used to catch contextMenu events

        Args:
            item : QGraphicsItem
            event : QEvent

        Returns:
            True to filter, False to pass the event to the item
        """
        if type(event) == QtGui.QGraphicsSceneContextMenuEvent:
            self.contextMenuEvent(event)
            return True
        return False

    def serialize(self):
        ports = []
        for port in self.ports:
            ports.append({'name': port.name,
                          'orientation': port.orientation})
        definition = { 'ports' : ports ,
                       'pos' : [self.pos().x(),self.pos().y()]}
        return definition

    @property
    def typeName(self):
        return self.typeTextItem.text()

    @typeName.setter
    def typeName(self, value):
        self.typeTextItem.setText(value)

    def updatePath(self):
        raise NotImplementedError("This method must be defined in a subclass")


class BoxNode(Node):

    horizMargin = 15
    vertMargin = 5
    portSpacing = 4

    minWidth = 50
    minHeight = 50

    def __init__(self, instanceName, typeName, parent=None):
        Node.__init__(self, instanceName, typeName, parent=parent)

        self.width = self.minWidth
        self.height = self.minHeight
        self.updatePath()

    def updatePath(self):
        # Calculate the width
        width = self.minWidth
        height = self.vertMargin
        for port in self.ports:
            w = port.textWidth + self.horizMargin
            if w > width:
                width = w
            height += port.portHeight + self.portSpacing
        if len(self.ports) > 0:
            height += self.ports[-1].portHeight / 2.
        height += self.vertMargin
        self.width = width

        # Calculate the height, and place ports as you go
        y = self.vertMargin
        for port in self.ports:

            y += self.portSpacing + port.portHeight / 2.

            #height = port.boundingRect().height()
            if port.orientation == PortOrientation.Right:
                port.setPos(self.width / 2., y)
            else:
                port.setPos(-self.width / 2., y)

            y += port.portHeight / 2.

        if height > self.minHeight:
            self.height = height
        else:
            self.height = self.minHeight

        typeFontMetrics = QtGui.QFontMetrics(self.typeTextItem.font())
        instanceFontMetrics = QtGui.QFontMetrics(self.instanceTextItem.font())

        self.typeTextItem.setPos(-self.width/2, self.height)
        self.instanceTextItem.setPos(-self.width/2, -instanceFontMetrics.height() - self.vertMargin / 2.)

        p = QtGui.QPainterPath()
        p.addRoundedRect(-self.width / 2, 0.0 , self.width, self.height, 5, 5)
        self.setPath(p)
