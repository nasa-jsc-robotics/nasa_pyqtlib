from pyqtlib import QtGui, QtCore, QT_LIB

from .Element import Element
from .TextItem import TextItem

class PortOrientation(object):
    Right = 0
    Up    = 1
    Left  = 2
    Down  = 3

class PortDirection(object):
    Input = 0
    Output = 1

def createRequiresPath(radius, direction, stub_length, pen_width):
    p = QtGui.QPainterPath()
    p.moveTo(direction * pen_width / 2., 0)
    p.lineTo(QtCore.QPointF((direction * stub_length), 0.0))
    rect = QtCore.QRectF( direction*(stub_length + radius) - radius, -radius, 2 * radius, 2 * radius)
    startAngle = 90.
    spanAngle = direction * 180.
    p.arcMoveTo(rect, 90)
    p.arcTo(rect, startAngle, spanAngle)
    return p

def createProvidesPath(radius, direction, stub_length, pen_width):
    p = QtGui.QPainterPath()
    p.moveTo(direction * pen_width/2., 0)
    p.lineTo(QtCore.QPointF(direction * (stub_length), 0.0))
    p.addEllipse(QtCore.QPointF(direction * (stub_length + radius), 0.0), radius, radius)
    return p

class Port(Element):

    portHeight = 15

    def __init__(self, name='', orientation=PortOrientation.Right, parent=None):
        Element.__init__(self, name, parent)

        self.label = TextItem(name)
        self.label.setParentItem(self)
        self.label.setEditable(False)
        self.label.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)
        self.label.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)

        self.setAcceptHoverEvents(True)


        if QT_LIB is 'PyQt4':
            self.setHandlesChildEvents(True)

        self.connections = []
        self.connectable = True
        self.focusable = True
        self.highlight = False

        self._connectPoint = QtCore.QPointF(0.0, 0.0)
        self._orientation = orientation
        self.name = name
        self.updatePath()

        self.highLightEffect = QtGui.QGraphicsColorizeEffect()
        self.highLightEffect.setEnabled(False)
        self.highLightEffect.setColor(QtCore.Qt.darkRed)
        self.setGraphicsEffect(self.highLightEffect)

    def boundingRect(self):
        """
        By overloading the "boundingRect" we can select the label and the port graphic
        Returns:
            QRectF
        """
        pathBox = QtGui.QGraphicsPathItem.shape(self).boundingRect()
        textBox = self.label.mapRectToParent(self.label.shape().boundingRect())
        totalBox = pathBox | textBox
        unitedBox = totalBox.united(textBox)
        return unitedBox

    def connect(self, connection):
        if connection not in self.connections:
            self.connections.append(connection)
            self.updatePath()

    @property
    def connected(self):
        return self.numConnections > 0

    def contextMenuEvent(self, event):
        menu = self.getContextMenu(event)
        if menu is not None:
            event.accept()
            menu.exec_(event.screenPos())

    def hoverEnterEvent(self, QGraphicsSceneHoverEvent):
        self.highLightEffect.setEnabled(True)

    def hoverLeaveEvent(self, QGraphicsSceneHoverEvent):
        self.highLightEffect.setEnabled(False)

    def deserialize(self, definition):
        self.setPos(*definition['pos'])
        self.orientation = definition['orientation']

    def disconnect(self, connection):
        if connection in self.connections:
            self.connections.remove(connection)
            self.updatePath()

    def disconnectAll(self):
        for connection in self.connections:
            connection.disconnect()

    def getContextMenu(self, event):
        """
        Create and return the context menu for this item.

        Args:
            event: QGraphicsSceneContextMenuEvent

        Returns:
            QtGui.QMenu or None
        """
        menu = QtGui.QMenu('Port')
        if self.orientation == PortOrientation.Right:
            action = menu.addAction('Set orientation left')
            action.triggered.connect(self.setOrientationToLeft)
        else:
            action = menu.addAction('Set orientation right')
            action.triggered.connect(self.setOrientationToRight)

        return menu

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
        self.label.setText(self._name)
        self.updatePath()

    @property
    def numConnections(self):
        return len(self.connections)

    @property
    def orientation(self):
        return self._orientation

    @orientation.setter
    def orientation(self, value):
        self._orientation = value
        self.updatePath()
        if self.parentItem() is not None:
            parent = self.parentItem()
            if hasattr(parent, 'updatePath'):
                parent.updatePath()

    def serialize(self):
        definition = { 'pos' : [self.pos().x(), self.pos().y()],
                       'orientation' : self.orientation}
        return definition

    def setOrientationToRight(self):
        self.orientation = PortOrientation.Right

    def setOrientationToLeft(self):
        self.orientation = PortOrientation.Left

    @property
    def textHeight(self):
        fm = QtGui.QFontMetrics(self.label.font())
        return fm.height()

    @property
    def textWidth(self):
        fm = QtGui.QFontMetrics(self.label.font())
        return fm.width(self.label.text())

    def updatePath(self):
        raise NotImplementedError("This method must be defined in a subclass")

class CirclePort(Port):

    radius = 5
    margin = 2

    def updatePath(self):
        p = QtGui.QPainterPath()

        if self.orientation == PortOrientation.Right:
            dir = 1.
        else:
            dir = -1.

        p.addEllipse( dir * self.radius - self.radius, -self.radius, 2 * self.radius, 2 * self.radius)
        self.connectPoint = QtCore.QPointF(dir * self.radius, 0.0)
        self.setPath(p)
        self.setPen(QtGui.QPen(QtCore.Qt.darkRed))
        self.setBrush(QtCore.Qt.red)

        if self.orientation == PortOrientation.Right:
            self.label.setPos(-self.label.boundingRect().width(),
                              -self.label.boundingRect().height() / 2)
        else:
            self.label.setPos(0, -self.label.boundingRect().height() / 2)

class ProvidesPort(Port):

    stub_length = 10
    pen_width = 2
    radius = 5

    def updatePath(self):

        if self.orientation == PortOrientation.Right:
            dir = 1.
        else:
            dir = -1.

        radius = self.radius - self.pen_width

        if self.connected:
            p = createProvidesPath(0, dir, self.stub_length, self.pen_width)
            self.connectPoint = QtCore.QPointF(dir * (self.stub_length), 0.0)

        else:
            p = createProvidesPath(radius, dir, self.stub_length, self.pen_width)
            self.connectPoint = QtCore.QPointF(dir * (radius + self.stub_length), 0.0)

        self.setPath(p)
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setWidth(self.pen_width)
        self.setPen(pen)
        self.setBrush(QtCore.Qt.black)

        if self.orientation == PortOrientation.Right:
            self.label.setPos(-self.label.boundingRect().width(),
                              -self.label.boundingRect().height() / 2)
        else:
            self.label.setPos(0, -self.label.boundingRect().height() / 2)

class RequiresPort(Port):

    stub_length = 10
    pen_width = 2
    radius = 3

    def __init__(self, name='', orientation=PortOrientation.Right, parent=None):
        Port.__init__(self, name, orientation, parent)
        self.connectedPath = None

    def updatePath(self):
        radius = self.radius + 2 * self.pen_width

        if self.orientation == PortOrientation.Right:
            dir = 1.
        else:
            dir = -1.

        self.connectPoint = QtCore.QPointF(dir * (radius + self.stub_length), 0.0)

        p = createRequiresPath(radius, dir, self.stub_length, self.pen_width)

        if self.connected:
            self.connectedPath = QtGui.QPainterPath()
            self.connectedPath.addEllipse(self.connectPoint, self.radius, self.radius)
        else:
            self.connectedPath = None

        self.setPath(p)
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setWidth(self.pen_width)
        self.setPen(pen)

        if self.orientation == PortOrientation.Right:
            self.label.setPos(-self.label.boundingRect().width(),
                              -self.label.boundingRect().height() / 2)
        else:
            self.label.setPos(0, -self.label.boundingRect().height() / 2)

    def paint(self, painter, QStyleOptionGraphicsItem, QWidget_widget=None):
        """
        Args:
            painter : QPainter:
            QStyleOptionGraphicsItem:
            QWidget_widget:
        """
        if self.connected:
            Port.paint(self, painter, QStyleOptionGraphicsItem, QWidget_widget)
            painter.setBrush(QtCore.Qt.black)
            painter.drawPath(self.connectedPath)
        else:
            Port.paint(self, painter, QStyleOptionGraphicsItem, QWidget_widget)

class ArrowOutPort(Port):

    arrow_width = 5
    pen_width = 2

    def updatePath(self):
        p = QtGui.QPainterPath()

        if self.orientation == PortOrientation.Right:
            dir = 1.0
        else:
            dir = -1.0

        p.moveTo(dir * self.pen_width / 2., self.arrow_width)
        p.lineTo(dir * (self.arrow_width + self.pen_width / 2.), 0)
        p.lineTo(dir * self.pen_width / 2., -self.arrow_width)

        self.connectPoint = QtCore.QPointF(dir * self.arrow_width, 0)
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setWidth(self.pen_width)
        self.setPen(pen)

        self.setPath(p)

        if self.orientation == PortOrientation.Right:
            self.label.setPos(-self.label.boundingRect().width(),
                              -self.label.boundingRect().height() / 2)
        else:
            self.label.setPos(0, -self.label.boundingRect().height() / 2)

class ArrowInPort(Port):

    arrow_width = 5
    pen_width = 2

    def updatePath(self):
        p = QtGui.QPainterPath()

        if self.orientation == PortOrientation.Right:
            dir = 1.0
        else:
            dir = -1.0

        p.moveTo(dir * (self.arrow_width + self.pen_width / 2.), self.arrow_width)
        p.lineTo(dir * self.pen_width / 2., 0)
        p.lineTo(dir * (self.arrow_width + self.pen_width / 2.), -self.arrow_width)

        self.connectPoint = QtCore.QPointF(dir * self.arrow_width, 0)
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setWidth(self.pen_width)
        self.setPen(pen)

        self.setPath(p)

        if self.orientation == PortOrientation.Right:
            self.label.setPos(-self.label.boundingRect().width(),
                              -self.label.boundingRect().height() / 2)
        else:
            self.label.setPos(0, -self.label.boundingRect().height() / 2)
