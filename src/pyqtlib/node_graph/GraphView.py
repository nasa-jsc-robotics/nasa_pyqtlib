from pyqtlib import QtGui, QtCore
from pyqtlib import QT_LIB

import logging

from .Element import Element

logger = logging.getLogger(__name__)


class GraphView(QtGui.QGraphicsView):
    """
    The GraphView class is a sub-class of the QGraphicsView specifically written to be used with the
    GraphScene.  The GraphScene/GraphView was written build a collection of nodes and edges like one
     would see in a graph.

     This class handles panning and zooming the scene.  The user can pan with a double-left-click-and-hold and with middle-click-and-hold.
     The user can zoom in/out by pressing and holding the Ctrl key while moving the mouse wheel.
    """

    MouseScaleFactor = 0.2
    ScrollbarLimitTolerace = 5

    itemClicked = QtCore.pyqtSignal('QGraphicsItem')
    itemDoubleClicked = QtCore.pyqtSignal('QGraphicsItem')

    def __init__(self, parent=None):
        super(GraphView, self).__init__(parent)

        self.setResizeAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.setAcceptDrops(True)
        self.setViewportUpdateMode(QtGui.QGraphicsView.FullViewportUpdate)

        self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)
        self.setResizeAnchor(QtGui.QGraphicsView.AnchorUnderMouse)

        self.zoomInAction = QtGui.QAction("Zoom in", self)
        self.zoomInAction.triggered.connect(self.zoomIn)
        self.zoomInAction.setShortcut(QtGui.QKeySequence.ZoomIn)

        self.zoomOutAction = QtGui.QAction("Zoom out", self)
        self.zoomOutAction.triggered.connect(self.zoomOut)
        self.zoomOutAction.setShortcut(QtGui.QKeySequence.ZoomOut)

        self.resetZoomAction = QtGui.QAction("Reset zoom", self)
        self.resetZoomAction.triggered.connect(self.resetZoom)

        self.undoAction = QtGui.QAction("Undo", self, triggered=self.undo)
        self.undoAction.setShortcut(QtGui.QKeySequence.Undo)
        self.redoAction = QtGui.QAction("Redo", self, triggered=self.redo)
        self.redoAction.setShortcut(QtGui.QKeySequence.Redo)

        self.addAction(self.zoomInAction)
        self.addAction(self.zoomOutAction)
        self.addAction(self.resetZoomAction)
        self.addAction(self.undoAction)
        self.addAction(self.redoAction)

        self._isPanning = False
        self._panPos = None

        self._connection = None

        self.show()

    def contextMenuEvent(self, event):
        """
        ContextMenu Handler.

        If there is no item under the cursor, a view menu will appear.

        Args:
            event : QContextMenuEvent
        """
        menu = self.getContextMenu(event)
        if menu is not None:
            menu.exec_(event.globalPos())
            event.accept()
            return
        super(GraphView, self).contextMenuEvent(event)

    def getContextMenu(self, event):
        if self.itemAt(event.pos()) is None:
            menu = QtGui.QMenu()
            menu.addActions(self.getZoomContextMenu(event).actions())
            menu.addSeparator()
            menu.addActions(self.getUndoRedoContextMenu(event).actions())
            return menu
        else:
            return None

    def getZoomContextMenu(self, event):
        menu = QtGui.QMenu('Zoom')
        menu.addAction(self.zoomInAction)
        menu.addAction(self.zoomOutAction)
        menu.addAction(self.resetZoomAction)
        return menu

    def getUndoRedoContextMenu(self, event):
        menu = QtGui.QMenu('Undo/Redo')
        menu.addAction(self.undoAction)
        menu.addAction(self.redoAction)
        return menu

    def mousePressEvent(self, event):
        """
        Event handler for single-click mouse events.
        
        This overloaded method allows the user to pan by 
        middle-clicking the mouse and dragging

        @type event QMouseEvent
        @parameter event Incoming mouse event
        """
        if event.buttons() == QtCore.Qt.MiddleButton:
            items = self.items(event.pos())
            for item in items:
                if isinstance(item, Element):
                    self.itemClicked.emit(item)
                    event.accept()
                    return
            self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
            self._panPos = event.pos()
            event.accept()
            return
        super(GraphView, self).mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        """
        Event handler for double-click mouse events.

        This overloaded method allows the user to pan by
        double-clicking the left mouse button and dragging

        @type event QMouseEvent
        @parameter event Incoming mouse event
        """
        if event.buttons() == QtCore.Qt.LeftButton:
            items = self.items(event.pos())
            for item in items:
                if item is not None:
                    if isinstance(item, Element):
                        self.itemDoubleClicked.emit(item)
                    super(GraphView, self).mouseDoubleClickEvent(event)
                    return
            self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
            self._panPos = event.pos()
            event.accept()
            return
        super(GraphView, self).mouseDoubleClickEvent(event)

    def mouseMoveEvent(self, event):
        """
        Event handler for mouse move events

        When the Canvas is in Canvas.MouseMode.Connect and the
        left mouse button is pressed, the connector will update it's
        endpoint position with the current position of the cursor.

        If the Canvas is currently being panned, this event will update
        the scroll bars and resize the scene to reflect the new position.
        This is necessary because a QGraphicsView does not let you scroll
        outside of the bounds of the sceneRect.

        @type event QMouseEvent
        @parameter event Incoming mouse event
        """
        if self.dragMode() == QtGui.QGraphicsView.ScrollHandDrag:
            diff = event.pos() - self._panPos
            self._panPos = event.pos()
            self.horizontalScrollBar().setValue(self.horizontalScrollBar().value() - diff.x())
            self.verticalScrollBar().setValue(self.verticalScrollBar().value() - diff.y())
            sceneRect = self.sceneRect()

            # Check if we are at the edge of the vertical scroll bars.  If we are, resize the scene so we can pan around
            if (abs(self.verticalScrollBar().value() - self.verticalScrollBar().minimum()) < GraphView.ScrollbarLimitTolerace) or \
               (abs(self.verticalScrollBar().value() - self.verticalScrollBar().maximum()) < GraphView.ScrollbarLimitTolerace):
                self.setSceneRect(sceneRect.x(), sceneRect.y() - diff.y(), sceneRect.width(), sceneRect.height() + abs(diff.y()))

            # set this variable again to handle panning in both x and y during the same event.
            sceneRect = self.sceneRect()

            # Check if we are at the edge of the horizontal scroll bars.  If we are, resize the scene so we can pan around
            if (abs(self.horizontalScrollBar().value() - self.horizontalScrollBar().minimum()) < GraphView.ScrollbarLimitTolerace) or \
               (abs(self.horizontalScrollBar().value() - self.horizontalScrollBar().maximum()) < GraphView.ScrollbarLimitTolerace):
                self.setSceneRect(sceneRect.x() - diff.x(), sceneRect.y(), sceneRect.width() + abs(diff.x()), sceneRect.height())
            event.accept()
            return
        super(GraphView, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton or event.button() == QtCore.Qt.MiddleButton:
            if self.dragMode() == QtGui.QGraphicsView.ScrollHandDrag:
                self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)
                event.accept()
                return
        super(GraphView, self).mouseReleaseEvent(event)

    def redo(self):
        if self.scene() is not None:
            self.scene().redo()

    def resetZoom(self):
        self.resetTransform()

    def wheelEvent(self, event):
        """
        overload the wheelEvent handler to enable zoom-scroll with the mouse wheel
        """

        if event.modifiers() == QtCore.Qt.ControlModifier:
            scaleFactor = 1

            if QT_LIB == 'PyQt4':
                delta = event.delta()
            elif QT_LIB == 'PyQt5':
                delta = event.angleDelta().y()
            else:
                super(GraphView, self).wheelEvent(event)
                return

            print delta
            if delta > 0:
                self.zoomIn()
            elif delta < 0:
                self.zoomOut()

            event.accept()
        else:
            super(GraphView,self).wheelEvent(event)

    def undo(self):
        if self.scene() is not None:
            self.scene().undo()

    def zoomIn(self):
        sf = 1 + GraphView.MouseScaleFactor
        self.scale(sf, sf)

    def zoomOut(self):
        sf = 1 - GraphView.MouseScaleFactor
        self.scale(sf, sf)
