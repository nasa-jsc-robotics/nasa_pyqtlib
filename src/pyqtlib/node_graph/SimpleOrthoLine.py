from pyqtlib import QtGui, QtCore

from pyqtlib.node_graph import Element
from pyqtlib.node_graph.Connection import Connection


class DiamondHandle(Element):
    """
    The DiamondHandle is a small diamond that can be used as a handle for orthogonal movement.  When the orthogonalMove
    property is set to True, this Element will only move along the x direction when dragged during a mouse move event.
    """

    width = 6
    height = 8

    def __init__(self, parent=None):
        super(DiamondHandle, self).__init__(parent=parent)

        self._orthogonalMove = True

        # storage for calculating orthogonal movement
        self.mousePressStartPoint = self.scenePos()
        self.posWhenMousePressed = self.scenePos()
        self.updatePath()

        self.buddies = set()

    @property
    def orthogonalMove(self):
        return self._orthogonalMove

    @orthogonalMove.setter
    def orthogonalMove(self, value):
        self._orthogonalMove = value

    def mousePressEvent(self, event):
        """
        Override to capture mouse press events

        Args:
            event: QGraphicsSceneMouseEvent
        """
        self.mousePressStartPoint = event.scenePos()
        self.posWhenMousePressed = self.scenePos()
        super(DiamondHandle, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        """
        Override to capture mouse move events

        Args:
            event: QGraphicsSceneMouseEvent
        """
        oldPos = self.pos()
        if self.orthogonalMove:
            dx = event.scenePos().x() - self.mousePressStartPoint.x()
            dy = event.scenePos().y() - self.mousePressStartPoint.y()
            new_x = event.scenePos().x()
            new_y = self.posWhenMousePressed.y()

            self.setPos(new_x, new_y)
        else:
            super(DiamondHandle, self).mouseMoveEvent(event)

        self.parentItem().childMoved(self, oldPos, self.pos())

    def updatePath(self):
        """
        Draw the path, which is a yellow diamond
        """
        p = QtGui.QPainterPath()

        p.moveTo(-DiamondHandle.width / 2.0, 0.0)
        p.lineTo(0.0, DiamondHandle.height / 2.0)
        p.lineTo(DiamondHandle.width / 2.0, 0.0)
        p.lineTo(0.0, -DiamondHandle.height / 2.0)
        p.lineTo(-DiamondHandle.width / 2.0, 0.0)

        self.setPath(p)
        self.setPen(QtGui.QPen(QtCore.Qt.darkYellow))
        self.setBrush(QtCore.Qt.yellow)


class SimpleOrthoLine(Connection):
    """
    The SimpleOrthoLine connection will draw three orthogonal lines between the startPoint and the endPoint.  The first
    segment is always horizontal, the second line is vertical, and the third segment is horizontal.  By default, the
    verticle section will be half-way between the startPoint's and endPoint's x coordinate.  Once the vertical handle
    has moved, however, the vertical position is saved.  By calling resetHandle, the handle will track the center-point
    again.
    """

    def __init__(self, parent=None):
        super(SimpleOrthoLine, self).__init__(parent=parent)
        self.diamondHandle = DiamondHandle(parent=self)
        self.diamondMoved = False
        self.setAcceptHoverEvents(True)

    def hoverEnterEvent(self, event):
        """
        Overload to handle hover events.  This is used to show the diamondHandle.

        Args:
            event: QGraphicsSceneHoverEvent
        """
        self.diamondHandle.show()
        self.update()

    def hoverLeaveEvent(self, event):
        """
        Overload to handle hover events.  This is used to hide the diamondHandle.

        Args:
            event: QGraphicsSceneHoverEvent
        """
        self.diamondHandle.hide()
        self.update()

    def childMoved(self, item, oldPos, newPos):
        """
        Callback to let this item know the diamond has moved.

        Args:
            item: item that moved
            oldPos: position before the move
            newPos: position after the move
        """
        self.diamondMoved = True

    def deserialize(self, definition):
        """
        Deserialize this object.  Useful when constructing the object from a file

        Args:
            definition: python dictionary
        """
        if self.diamondMoved:
            self.setHandlePos(QtCore.QPointF(definition['handlePos']['x'], definition['handlePos']['y']))

    def resetHandle(self):
        """
        Reset the diamond handle so that it automatically tracks the center x coordinate.
        """
        self.diamondMoved = False
        self.update()

    def contextMenuEvent(self, event):
        """
        Override to handle context menu events.

        Args:
            event: QGraphicsSceneContextMenuEvent
        """
        menu = self.getContextMenu(event)
        if menu is not None:
            menu.exec_(event.screenPos())

    def getContextMenu(self, event):
        """
        Create and return a context menu for this item.

        Args:
            event: QGraphicsSceneContextMenuEvent

        Returns:
            QMenu object populated with actions.
        """
        menu = QtGui.QMenu()
        resetHandleAction = menu.addAction('Reset Handle')
        resetHandleAction.triggered.connect(lambda: self.resetHandle())
        return menu

    def paint(self, painter, option, widget):
        """
        Override to paint this object.

        Args:
            painter: QPainter
            option: QStyleOptionGraphicsItem
            widget: QWidget
        """
        startPoint = self.startPoint
        endPoint = self.endPoint

        mid_x = (endPoint.x() + startPoint.x()) / 2.0
        mid_y = (endPoint.y() + startPoint.y()) / 2.0

        if self.diamondMoved:
            diamond_x = self.diamondHandle.pos().x()
            diamond_y = mid_y
        else:
            diamond_x = mid_x
            diamond_y = mid_y

        self.diamondHandle.setPos(diamond_x, diamond_y)

        self.path = QtGui.QPainterPath(startPoint)
        self.path.lineTo(diamond_x, startPoint.y())
        self.path.lineTo(diamond_x, endPoint.y())
        self.path.lineTo(endPoint.x(), endPoint.y())
        self.setPath(self.path)
        super(SimpleOrthoLine, self).paint(painter, option, widget)

    def serialize(self):
        """
        Return a dictionary of values to reconstruct this object.

        Passing the dictionary returned by this method to deserialize() will put this object into that state.

        Returns:
            Python dictionary
        """
        definition = super(SimpleOrthoLine, self).serialize()
        definition['handleMoved'] = self.diamondMoved
        definition['handlePos'] = {'x': self.diamondHandle.pos().x(),
                                   'y': self.diamondHandle.pos().y()}
        return definition

    def setHandlePos(self, point):
        """
        Set the position of the diamond handle.

        Args:
            point: position as a QtCore.QPointF
        """
        self.diamondMoved = True
        self.diamondHandle.setPos(point)

