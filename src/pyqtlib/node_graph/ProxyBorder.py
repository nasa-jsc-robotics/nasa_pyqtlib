from pyqtlib import QtGui, QtCore

from taskforce_model import TaskForceColors
from .BoxItem import BoxItem

class ProxyBorder(BoxItem):
    """
    The Proxy Border is a rounded box that acts as a resizeable container for a QGraphicsProxyWidget.  The proxy
    can be passed into the constructor, or set with "setProxy".  When setting the proxy, the border will resize itself
    around the current size of the widget.

    When mousing over the lower-righthand corner, the cursor will change to a diagonal re-sizing cursor, and the user
    can resize the border, which in-turn, will resize the widget.
    """
    def __init__(self, proxy=None, margin=15.):
        """
        Args:
            proxy: a QGraphicsProxyWidget
            margin: margin around the proxy widget (i.e. the border) in pixels
        """
        super(ProxyBorder, self).__init__(-margin, -margin, 10+2.*margin, 10+2.*margin)

        # to make resizing easier, let the upper-lefthand corner live at 0.0, 0.0.
        # and change the mouse scale factor to 1.0
        self.setPos(0.0, 0.0)
        self.setMouseResizeScaling(1.0)

        self.margin = margin
        self.proxy = None
        self.resize(10,10)
        self.color = TaskForceColors.ProxyBorderDefaultColor

        if proxy:
            self.setProxy(proxy)

    def fit_border_to_widget(self):
        """
        Resize the border to fit the current widget size.
        """
        if self.proxy:
            w = self.proxy.widget().width() + 2. * self.margin
            h = self.proxy.widget().height() + 2. * self.margin
            self.resize(w, h, fit_widget=False)

    def fit_widget_to_border(self):
        """
        Resize the widget to fit within the current border size.
        """
        if self.proxy:
            w = self.boundingRect().width() - 2. * self.margin
            h = self.boundingRect().height() - 2. * self.margin
            self.proxy.widget().resize(w, h)


    def resize(self, width, height, fit_widget=True):
        """
        Resize the border to "width" and "height".

        This method is called repeatedly during a mouseMove event when the user is resizing this item with the mouse.

        Args:
            width: new width in pixels
            height: new height in pixels
            fit_widget: if True, resize the contained proxy (default)
        """

        super(ProxyBorder, self).resize(width, height)
        if self.proxy:
            self.proxy.setPos(self.margin, self.margin)
        if fit_widget:
            self.fit_widget_to_border()

    def setProxy(self, proxy):
        """
        Set the proxy widget.

        Args:
            proxy: QGraphicsProxyWidget
        """
        self.proxy = proxy
        proxy.setParentItem(self)
        self.fit_border_to_widget()
