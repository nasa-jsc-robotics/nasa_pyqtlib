from pyqtlib import QtGui, QtCore

from .scene_undo import SceneAddItem, SceneRemoveItem, SceneMoveItem
from .Connection import Connection
from .MultiStyleConnection import MultiStyleConnection
from .Port import Port


class GraphScene(QtGui.QGraphicsScene):
    """
    This is a subclass of the QtGui.QGraphicsScene.

    One motivation for this subclass is to overload
    the event handlers to better facilitate the undo/redo
    framework.

    The itemsMoved signal can be used to record the position of
    items that have moved in the scene.  The argument of the signal
    is a list of tuples, where each tuple is an item and a position:
        [(QGraphicsItem, QPointF)]
    """

    # signal emitted when two items have been connected using the manual user drawing interface
    connectionDrawComplete = QtCore.pyqtSignal('QGraphicsItem', 'QGraphicsItem', 'QGraphicsItem')
    dragAndDropEvent = QtCore.pyqtSignal(QtGui.QGraphicsSceneDragDropEvent)

    class MouseMode:
        Select  = 0
        Connect = 1

    def __init__(self, parent=None):
        super(GraphScene, self).__init__(parent)
        # list of tuples, where each tuple is (item, startPosition)
        self.movingItemsStartPositions = []
        self.undoStack = QtGui.QUndoStack(parent=self)

        self.setMouseMode(GraphScene.MouseMode.Select)
        self._drawingConnection = False
        self._isPanning = False

        self.connectionType = MultiStyleConnection
        self.newConnectionArgs = ()
        self.newConnectionKwargs = {'style': MultiStyleConnection.ConnectionStyle.Line}
        self.connection = None
        self.startItem = None
        self.endItem = None

    def _recordItemsMoved(self, movingItemsStartPositions):
        """
        Record moves from the Scene to the undo stack.

        This is a private slot.
        """
        if len(movingItemsStartPositions) > 1:
            self.undoStack.beginMacro('Move items')

        for item, startPos in movingItemsStartPositions:
            if hasattr(item, "text"):
                text = item.text
            else:
                text = type(item)
            description = 'move item {} from ({},{}) to ({},{})'.format(text, startPos.x(), startPos.y(), item.pos().x(), item.pos().y())
            command = SceneMoveItem(item, startPos, description)
            self.undoStack.push(command)
        if len(movingItemsStartPositions) > 1:
            self.undoStack.endMacro()

    def addItem(self, item, store_undo=False):
        if store_undo:
            self.undoStack.push(SceneAddItem(self, item, "add item"))
        else:
            QtGui.QGraphicsScene.addItem(self, item)

    def connectItems(self, startItem, endItem, connectionItem=None):
        """
        Connect two items.

        Args:
            startItem: Starting item
            endItem: Ending item
            connectionItem: ConnectionItem.  If None, a new object will be created.
        """
        if startItem == endItem:
            return None

        if not connectionItem:
            connectionItem = self.createConnectionItem(args=self.newConnectionArgs, kwargs=self.newConnectionKwargs)
        if startItem.connectable and endItem.connectable:
            connectionItem.connect(startItem, endItem)
            self.addItem(connectionItem)
            return connectionItem
        return None

    def connectionDrawUpdateEvent(self, event):
        """
        Update the connection endpoint.

        This method is called from the mouseMoveEvent while the connection is being updated
        :param event: Event when the position of the endpoint has changed
        :return:
        """
        self.connection.endPoint = QtCore.QPointF(event.scenePos())

    def connectionStartDrawingEvent(self, event):
        """
        Start drawing a connection.

        This method is called by the mousePressEvent to start drawing a connection

        :param event:  Event called when the mousePressEvent happened
        """
        for item in self.items(event.scenePos()):
            if hasattr(item, "connectable"):
                if item.connectable:
                    self._drawingConnection = True
                    self.startItem = item
                    self.connection = self.createConnectionItem(*self.newConnectionArgs, **self.newConnectionKwargs)
                    self.addItem(self.connection, store_undo=False)
                    self.connection.startPoint = self.startItem.connectPointInScene()
                    return True

    def connectionContinueDrawingEvent(self, event):
        """
        Continue drawing a connection.  This method is when continuing a draw action, such as adding an additional
        vertex to a line or spline.

        Args:
            event: Event called when the mousePressEvent happened

        Returns:

        """
        # First, check to see if we are on another connectable item.  If so, connect.
        items = self.items(event.scenePos())
        for item in items:
            if hasattr(item, "connectable"):
                if item.connectable:
                    return self.connectionStopDrawingEvent(event)

        return self.connection.continueEvent(event)

    def connectionStopDrawingEvent(self, event):
        """
        Stop drawing a connection.

        This method is called by the mouseReleaseEvent
        :param event: Event that was called when the connection was stopped
        :return:
        """
        self._drawingConnection = False
        items = self.items(event.scenePos())
        for item in items:
            if hasattr(item, "connectable"):
                if item.connectable:
                    startItem = self.startItem
                    endItem = item

                    connectionItem = self.connectItems(startItem, endItem, self.connection)
                    if connectionItem:
                        self.connectionDrawComplete.emit(startItem, endItem, connectionItem)
                    return
        self.removeItem(self.connection)

    def createConnectionItem(self, *args, **kwargs):
        connection = self.connectionType(*args, **kwargs)
        return connection

    def dragEnterEvent(self, event):
        # default behavior for dragEnterEvent is to ignore it
        event.accept()

    def dragMoveEvent(self, event):
        # default behavior for dragMoveEvent is to ignore it
        event.accept()

    def dragLeaveEvent(self, event):
        # default behavior for dragLeaveEvent is to ignore it
        event.accept()

    def dropEvent(self, event):
        """
        Emit a signal when receiving a drop event
        Args:
            event: QGraphicsSceneDragDropEvent:
        """
        self.dragAndDropEvent.emit(event)

    def getConnections(self, item=None):
        """
        Return all connections connected to item.  If is None, return all connections.
        """
        items = []
        for connectionItem in self.getTopLevelItems():
            if isinstance(connectionItem, Connection):
                if not item:
                    items.append(connectionItem)
                elif connectionItem.isConnectedToItem(item):
                    items.append(connectionItem)
        return items

    def getTopLevelItems(self):
        items = []
        for item in self.items():
            if not item.parentItem():
                items.append(item)
        return items

    def getUndoStack(self):
        """
        Get the QUndoStack used by this class

        @type return QtGui.QUndoStack
        @return The QUndoStack
        """
        return self.undoStack

    def keyPressEvent(self, event):
        """
        Event handler for key presses.

        This method has been overloaded to allow users to remove items by
        using the "delete" or "backspace" keys on their keyboard

        @type event QKeyEvent
        @parameter event The key event
        """
        if event.key() in [QtCore.Qt.Key_Delete, QtCore.Qt.Key_Backspace]:
            focusItem = self.focusItem()

            # If a QGraphicsTextItem is being edited, do not capture the keypress.
            # If it is captured, the user cannot use delete or backspace while editing.
            if not isinstance(focusItem, QtGui.QGraphicsTextItem):
                items = self.selectedItems()
                itemsToRemove = []
                for item in items:
                    # only delete top-level items
                    #if not item.parentItem():
                    if hasattr(item, 'deleteable'):
                        if item.deleteable:
                            itemsToRemove.append(item)
                    else:
                        itemsToRemove.append(item)
                if len(itemsToRemove) > 0:
                    self.removeItems(itemsToRemove)
                return
        elif event.matches(QtGui.QKeySequence.Undo):
            self.undo()
            event.accept()
            return
        elif event.matches(QtGui.QKeySequence.Redo):
            self.redo()
            event.accept()
            return

        if event.key() == QtCore.Qt.Key_Escape:
            if self._drawingConnection:
                self._drawingConnection = False
                self.removeItem(self.connection)
                return

        super(GraphScene, self).keyPressEvent(event)

    def mouseMoveEvent(self, event):
        """
        Event handler for mouse move events

        When the Scene is in Canvas.MouseMode.Connect and the
        left mouse button is pressed, the connector will update it's
        endpoint position with the current position of the cursor.

        @type event QMouseEvent
        @parameter event Incoming mouse event
        """
        # Update the connection when we are drawing
        if self._drawingConnection:
            self.connectionDrawUpdateEvent(event)
            event.accept()
            return

        super(GraphScene, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        """
        overload mouse press event to capture selected items'
        starting position.
        """
        if event.button() == QtCore.Qt.LeftButton and self.mouseMode == GraphScene.MouseMode.Connect:
            if not self._drawingConnection:
                self.connectionStartDrawingEvent(event)
                event.accept()
                return

            if self._drawingConnection:
                self.connectionContinueDrawingEvent(event)
                event.accept()
                return

        if event.button() == QtCore.Qt.RightButton and self.mouseMode == GraphScene.MouseMode.Connect:
            if self._drawingConnection:
                self.connectionStopDrawingEvent(event)
                event.accept()
                return

        super(GraphScene, self).mousePressEvent(event)
        # Check all selected items.  If the left-button was clicked,
        # store the current position of those items in case they move
        movingItems = self.selectedItems()
        self.movingItemsStartPositions = []
        if movingItems != [] and event.button() == QtCore.Qt.LeftButton:
            for item in movingItems:
                self.movingItemsStartPositions.append((item, item.pos()))
            event.accept()

    def mouseReleaseEvent(self, event):
        """
        overload mouse release event
        """
        # if event.button() == QtCore.Qt.LeftButton:
        #     if self._drawingConnection:
        #         self.connectionStopDrawingEvent(event)
        #         event.accept()
        #         return

        # Handle moving items in the scene
        if self.movingItemsStartPositions != [] and event.button() == QtCore.Qt.LeftButton:
            for item, pos in self.movingItemsStartPositions:
                if item.pos() != pos:
                    self._recordItemsMoved(self.movingItemsStartPositions)
                    break
        super(GraphScene, self).mouseReleaseEvent(event)

    def redo(self):
        self.undoStack.redo()

    def removeItem(self, item, store_undo=False):
        if store_undo:
            self.undoStack.push(SceneRemoveItem(self, item, "remove item"))
        else:
            QtGui.QGraphicsScene.removeItem(self, item)

    def removeItems(self, items):
        for item in items:
            if isinstance(item, Port):
                item.disconnectAll()
            self.removeItem(item)

    def setConnectionType(self, name):
        self.connectionType = name

    def setNewConnectionArgs(self, args):
        self.newConnectionArgs = args

    def setNewConnectionKwargs(self, kwargs):
        self.newConnectionKwargs = kwargs

    def setMouseMode(self, mode):
        self.mouseMode = mode

    def setMouseModeConnect(self):
        print 'set mouse mode connect'
        self.setMouseMode(GraphScene.MouseMode.Connect)

    def setMouseModePointer(self):
        print 'set mouse mode select'
        self.setMouseMode(GraphScene.MouseMode.Select)

    def undo(self):
        self.undoStack.undo()