from pyqtlib import QtGui, QtCore, QT_LIB


class Element(QtGui.QGraphicsPathItem):

    def __init__(self, name=None, parent=None):
        QtGui.QGraphicsPathItem.__init__(self, parent=parent)
        
        if QT_LIB is 'PyQt4':
            self.setHandlesChildEvents(False)

        self.moveable = True
        self.focusable = True
        self.selectable = True

        self._name = name
        self.deleteable = True
        self.connectable = False
        self._connectPoint = QtCore.QPointF(0.0, 0.0)

    @property
    def connectPoint(self):
        return self._connectPoint

    @connectPoint.setter
    def connectPoint(self, value):
        self._connectPoint = value

    def connectPointInScene(self):
        return self.mapToScene(self._connectPoint)

    def deserialize(self, definition):
        pass

    def getContextMenu(self, event):
        """
            Create and return the context menu for this item.
            This should be overloaded in the child class.

            Args:
                event: QGraphicsSceneContextMenuEvent

            Returns:
                QtGui.QMenu or None
            """
        return None

    @property
    def focusable(self):
        return self.flags() | QtGui.QGraphicsItem.ItemIsSelectable

    @focusable.setter
    def focusable(self, value):
        self.setFlag(QtGui.QGraphicsItem.ItemIsFocusable, value)

    @property
    def moveable(self):
        return self.flags() | QtGui.QGraphicsItem.ItemIsMovable

    @moveable.setter
    def moveable(self, value):
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, value)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def selectable(self):
        return self.flags() | QtGui.QGraphicsItem.ItemIsSelectable

    @selectable.setter
    def selectable(self, value):
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, value)

    def serialize(self):
        return {}
        #raise NotImplementedError("This class must be implemented in a subclass")