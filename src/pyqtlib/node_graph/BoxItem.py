from pyqtlib import QtGui, QtCore

from .Element import Element

class BoxItem(Element):
    """
    A basic rectangle with rounded edges.
    """

    def __init__(self, x=0, y=0, width=50, height=50, name=None, parent=None):
        """
        The rectangle will be centered at x,y with a width=width and height=height

        Args:
            x: x position in the scene
            y: y position in the scene
            width: Width of the rectangle
            height: height of the rectangle
            parent: parent QtGraphicsItem
        """
        super(BoxItem, self).__init__(name=name, parent=parent)

        self._color = None

        self.width = width
        self.height = height

        self.penWidth = 1.
        self.original_width = width
        self.original_height = height
        self.start_resize_width = width
        self.start_resize_height = height
        self.resizeMargin = 10.
        self.minimum_width = 10.
        self.minumum_height = 10.
        self.mouseResizeScaleFactor = 1.0

        self.setAcceptHoverEvents(True)

        self.resizing = False
        self.in_resize_region = False

        self.start_resize_pos = QtCore.QPointF(0.0,0.0)
        self.updatePath()


    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        """
        Set the color of the object.

        Args:
            value: Color Value defined in the QtCore.Qt.Global Color enum
        """
        self._color = value
        self.setBrush(self._color)

    def contextMenuEvent(self, event):
        """

        Args:
            event: QGraphicsSceneContextMenuEvent

        Returns:

        """
        menu = self.getContextMenu()
        menu.exec_(event.screenPos())

    def getContextMenu(self, event):
        """
        Create and return the context menu for this item.

        Args:
            event: QGraphicsSceneContextMenuEvent

        Returns:
            QtGui.QMenu or None
        """
        menu = QtGui.QMenu()
        menu.addAction("Reset Size", self.reset_size)
        return menu

    def hoverMoveEvent(self, event):
        """
        Handle hoverMoveEvents.  This overload is used to change the icon to resize the item.

        Args:
            event: QGraphicsSceneHoverEvent
        """
        super(BoxItem, self).hoverMoveEvent(event)
        bottomRight = self.boundingRect().bottomRight()
        eventPos = event.pos()
        if (bottomRight.x() - eventPos.x()) < self.resizeMargin and (bottomRight.y() - eventPos.y()) < self.resizeMargin:
            self.setCursor(QtCore.Qt.SizeFDiagCursor)
            self.in_resize_region = True
        else:
            self.setCursor(QtCore.Qt.ArrowCursor)
            self.in_resize_region = False
        event.accept()

    def mouseMoveEvent(self, event):
        """
        Mouse move event handler.  If resizing, dynamically resize the item.

        Args:
            event: QGraphicsSceneMouseEvent
        """
        if self.resizing:
            delta_x = self.mouseResizeScaleFactor * (event.pos().x() - self.start_resize_pos.x())
            delta_y = self.mouseResizeScaleFactor * (event.pos().y() - self.start_resize_pos.y())

            width  = self.start_resize_width + delta_x
            height = self.start_resize_height + delta_y

            if width < self.minimum_width:
                width = self.minimum_width

            if height < self.minumum_height:
                height = self.minumum_height

            self.resize(width, height)
            event.accept()
            return

        super(BoxItem, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        """
        Handle mouse press events to start resizing the item.

        Args:
            event: QGraphicsSceneMouseEvent
        """
        if self.in_resize_region:
            self.resizing = True
            self.start_resize_pos = event.pos()
            self.start_resize_width = self.boundingRect().width()
            self.start_resize_height = self.boundingRect().height()
            self.start_resize_topLeft = self.boundingRect().topLeft()
            event.accept()
            return
        super(BoxItem, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        """
        Mouse release handler.  If resizing, self.resizing is set to False
        Args:
            event: QGraphicsSceneMouseEvent

        Returns:

        """
        if self.resizing:
            self.resizing = False
            event.accept()
            return
        super(BoxItem, self).mouseReleaseEvent(event)

    def resize(self, width, height):
        """
        Resize the box.

        Args:
            width: new width
            height: new height
        """
        self.width = width
        self.height = height
        self.updatePath()

    def reset_size(self):
        """
        Set the size back to the size set in the constructor.
        """
        self.resize(self.original_width, self.original_height)

    def setMouseResizeScaling(self, value):
        self.mouseResizeScaleFactor = value

    def updatePath(self):
        p = QtGui.QPainterPath()
        p.addRoundedRect(0.0, 0.0, self.width, self.height, 5, 5)
        self.setPath(p)
