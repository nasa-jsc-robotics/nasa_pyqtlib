from pyqtlib import QtGui

class SceneAddItem(QtGui.QUndoCommand):
    """
    QUndoCommand for adding an item to a Scene
    """
    def __init__(self, scene, item, description):
        """
        @type scene QtGui.QGraphicsScene
        @parameter scene Scene in which the item should be added

        @type item QGraphicsItem
        @parameter item Item that will be added

        @type description string
        @parameter description Description for the command
        """
        super(SceneAddItem, self).__init__(description)
        self.scene = scene
        self.item = item
        self.lastPos = self.item.pos()

    def redo(self):
        """
        Called by the QUndoStack to perform the command
        """
        QtGui.QGraphicsScene.addItem(self.scene, self.item)
        self.item.setPos(self.lastPos)

    def undo(self):
        """
        Called by the QUndoStack to undo the command
        """
        self.lastPos = self.item.pos()
        QtGui.QGraphicsScene.removeItem(self.scene, self.item)
        #self.scene._removeItem(self.item)

class SceneRemoveItem(QtGui.QUndoCommand):
    """
    QUndoCommand for removing an item to from Scene
    """
    def __init__(self, scene, item, description):
        """
        @type scene QtGui.QGraphicsScene
        @parameter scene Scene in which the item should be removed

        @type item QGraphicsItem
        @parameter item Item that will be removed

        @type description string
        @parameter description Description for the command
        """
        super(SceneRemoveItem, self).__init__(description)
        self.scene = scene
        self.item = item
        self.lastPos = self.item.pos()

    def redo(self):
        """
        Called by the QUndoStack to perform the command
        """
        self.lastPos = self.item.pos()
        QtGui.QGraphicsScene.removeItem(self.scene, self.item)
        #self.scene._removeItem(self.item)

    def undo(self):
        """
        Called by the QUndoStack to undo the command
        """
        QtGui.QGraphicsScene.addItem(self.scene, self.item)
        self.item.setPos(self.lastPos)
        #self.scene._addItem(self.item)

class SceneMoveItem(QtGui.QUndoCommand):
    """
    QUndoCommand for moving items on a Canvas
    """
    def __init__(self, item, startPos, description):
        """
        @type item QGraphicsItem
        @parameter item Item that moved

        @type startPos QPointF
        @parameter startPos Position before the move

        @type description string
        @parameter description Description for the command
        """
        super(SceneMoveItem,self).__init__(description)
        self.item = item
        self.startPos = startPos
        self.newPos = item.pos()

    def redo(self):
        """
        Called by the QUndoStack to perform the command
        """
        self.item.setPos(self.newPos)

    def undo(self):
        """
        Called by the QUndoStack to undo the command
        """
        self.item.setPos(self.startPos)
