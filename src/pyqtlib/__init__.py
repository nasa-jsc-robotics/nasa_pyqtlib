import sip

sip.setapi('QDate', 2)
sip.setapi('QDateTime', 2)
sip.setapi('QString', 2)
sip.setapi('QTextStream', 2)
sip.setapi('QTime', 2)
sip.setapi('QUrl', 2)
sip.setapi('QVariant', 2)

try:
    import PyQt5
    QT_LIB = 'PyQt5'

except:
    import PyQt4
    QT_LIB = 'PyQt4'

if QT_LIB is 'PyQt5':
    #  Importing only these modules from PyQt5 to reduce amount of imports.
    #  PyQt4 implementation imported all the modules which brought in a modules that
    #  are never used in our implementation
    from PyQt5 import QtGui, QtCore, QtWidgets, uic, QtTest

    QtGui.QApplication = QtWidgets.QApplication
    QtGui.QGraphicsScene = QtWidgets.QGraphicsScene
    QtGui.QGraphicsObject = QtWidgets.QGraphicsObject
    QtGui.QGraphicsWidget = QtWidgets.QGraphicsWidget
    QtGui.QSortFilterProxyModel = QtCore.QSortFilterProxyModel
    
    # Import all QtWidgets objects into QtGui
    for o in dir(QtWidgets):
        if o.startswith('Q'):
            setattr(QtGui, o, getattr(QtWidgets,o) )

    QtGui.QFileDialog.getOpenFileNameAndFilter = QtGui.QFileDialog.getOpenFileName

if QT_LIB is 'PyQt4':
    from PyQt4 import QtGui, QtCore, QtTest


from dialog import *
from log import *
from model import *
from plotter import *
from stylesheet import *
from table import *
from threads import *
from tree import *
from view import *

