import time
import re
import os

from pyqtlib import QtGui, QtCore

from log_table import LogTableWidget
from nasa_common_logging import parse_log_buffer

from pyqtlib.threads import WorkerThread

class LogFileView(LogTableWidget):
    """
    The LogFileView is an extension of the pyqtlib.LogTableWidget.  This widget
    will follow log files and add elements to the display when one of those files
    change.  It functions similarly to the "tail" command in Linux (with the the -f option to follow)

    """
    def __init__(self, paths=[], parent=None):
        """
        @type paths list-of-strings
        @param paths List of paths of log files

        @type parent QtGui.QWidget
        @param parent Parent widget
        """
        super(LogFileView,self).__init__(parent)
        self.fileWatcher = QtCore.QFileSystemWatcher()
        self.fileWatcher.fileChanged.connect(self.fileChanged)
        self.filePtrs = {}
        self.fileHasChanged = {}
        for path in paths:
            self.addFile(path)

        self.updateTimer = QtCore.QTimer()
        self.updateTimer.timeout.connect(self.updateLog)
        self.updateTimer.start(200)

    def addFile(self, path):
        """
        Add file to the watch list

        @type path string
        @param path Path of a log file
        """
        self.fileWatcher.addPath(str(path))
        filePtr = open(path,'r')
        filePtr.seek(0,os.SEEK_END)
        self.filePtrs[path] = filePtr
        self.fileHasChanged[path] = False

    def removeFile(self, path):
        """
        Stop watching a log file.

        @type path string
        @param path Path of file to stop following.
        """
        if path in self.filePtrs.keys():
            self.fileWatcher.removePath(path)
            self.filePtrs[path].close()
            del self.filePtrs[path]

    def updateLog(self):
        for path, changed in self.fileHasChanged.items():
            if changed:
                self.worker = WorkerThread(self.getEntries, path)
                self.worker.workerDone.connect(self.addEntries)
                self.worker.start()
                self.fileHasChanged[path] = False

    @QtCore.pyqtSlot(str)
    def fileChanged(self, path):
        """
        Mark a file as being changed.

        This method is connected to a QFileSystemWatcher.  This method gets
        called whenever the log file has changed and provides the name of the
        file

        @type path string
        @param path Filename of the path that changed.
        :return:
        """
        self.fileHasChanged[path] = True

    def getEntries(self, path):
        """
        Get new log entries out of a file.

        @type path string
        @param path Path to the file that changed

        @type list of tuple
        @return List of tuples of log entries
        """
        entries = []
        filePtr = self.filePtrs[path]
        buffer = str(filePtr.read())
        self.fileHasChanged[path] = False
        entries = parse_log_buffer(buffer)
        return entries


