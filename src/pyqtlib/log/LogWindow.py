from pyqtlib import QtGui, QtCore

import logging
import logging.handlers
import time

TIME_FMT_STR = '%Y-%m-%d %H:%M:%S'
LOG_FMT_STR = '%(asctime)s [%(levelname)-8s][%(name)s] %(message)s'

LOG_COLOR_DICT = {
	logging.DEBUG   : 'darkGreen',
	logging.INFO    : 'black',
	logging.WARNING : 'darkOrange',
	logging.ERROR   : 'red'
}

styleSheet = """
	QPlainTextEdit {
		font-size: 12px;
		font-family: "Courier New"
	}
"""


class LogWindowHandler(QtGui.QWidget, logging.Handler):

	logSignal = QtCore.pyqtSignal(str, str)

	def __init__(self,parent=None,level=logging.NOTSET):
		super(logging.Handler,self).__init__()
		super(QtGui.QWidget,self).__init__(parent)
		self.setLevel(logging.DEBUG)

	def handle(self, record):
		self.emit(record)

	def emit(self,record):
		logString = self.format(record)
		try:
			color = LOG_COLOR_DICT[record.levelno]
		except AttributeError:
			color = 'black'
		self.logSignal.emit(logString, color)


class LogWindow(QtGui.QWidget):
	def __init__(self, logger=None, parent=None):
		super(LogWindow,self).__init__(parent)

		# Setup the main text window
		self.edit = QtGui.QPlainTextEdit()
		self.edit.setPlainText('')
		self.edit.clear()
		self.edit.setFont(QtGui.QFont('TypeWriter', 10))

		# set buffer
		# TODO: allow to change at runtime
		self.edit.setMaximumBlockCount(20000)
		# not too small
		self.setMinimumHeight(100)

		####################
		# setup the logger, formatter, and handler
		self.logger = logger
		if self.logger == None:
			self.logger = logging.getLogger('')

		self.formatter = logging.Formatter(LOG_FMT_STR, TIME_FMT_STR)
		self.handler = LogWindowHandler()
		self.handler.setFormatter(self.formatter)
		self.logger.addHandler(self.handler)
		#
		#####################

		# setLogLevel actions
		self.setLogLevelDebugAction = QtGui.QAction('&Debug',self)
		self.setLogLevelInfoAction = QtGui.QAction('&Info',self)
		self.setLogLevelWarningAction = QtGui.QAction('&Warning',self)
		self.setLogLevelErrorAction = QtGui.QAction('&Error',self)

		self.setLogLevelDebugAction.triggered.connect(self.setLogLevelDebug)
		self.setLogLevelInfoAction.triggered.connect(self.setLogLevelInfo)
		self.setLogLevelWarningAction.triggered.connect(self.setLogLevelWarning)
		self.setLogLevelErrorAction.triggered.connect(self.setLogLevelError)

		# setLogLevelMenu
		self.setLogLevelMenu = QtGui.QMenu('Set Log Level')
		self.setLogLevelMenu.addAction(self.setLogLevelDebugAction)
		self.setLogLevelMenu.addAction(self.setLogLevelInfoAction)
		self.setLogLevelMenu.addAction(self.setLogLevelWarningAction)
		self.setLogLevelMenu.addAction(self.setLogLevelErrorAction)

		# Set the context menu policy for the right-click pop-up menu
		self.edit.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.edit.customContextMenuRequested.connect(self.showContextMenu)

		self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.customContextMenuRequested.connect(self.showContextMenu)

		# connect the handler's signal to this class' slot
		self.handler.logSignal.connect(self.addText)

		# setup the widget layout
		self.layout = QtGui.QHBoxLayout()
		self.layout.addWidget(self.edit)
		self.setLayout(self.layout)
		self.show()

	def showContextMenu(self, event):
		'''
		Create and show a custom context menu.  This event is connected to both
		the PlainTextEdit and overall widget's customContextMenuRequested signal
		'''
		editMenu = self.edit.createStandardContextMenu()
		editMenu.setTitle('E&dit')
		self.setLogLevelMenu.addSeparator()
		self.setLogLevelMenu.addMenu(editMenu)
		self.contextMenuEvent(event)

	def contextMenuEvent(self,event):
		'''
		This is the right-click pop-up menu actions

		@param event The event
		'''
		self.setLogLevelMenu.exec_(QtGui.QCursor.pos())

	@QtCore.pyqtSlot(str,str)
	def addText(self, text, color='black'):
		'''
		Add colored text to the window

		@type text string
		@param text Text to append

		@type color string
		@param color HTML color for the text.  Default is black
		'''
		html = '<font face="Courier New" color="{}">{}</font>'.format(color,str(text))
		self.edit.appendHtml(html)
		self.edit.moveCursor(QtGui.QTextCursor.End)

	def setLogLevel(self, level):
		'''
		Set the log level
		@param level Log level as defined my the python logging module
		'''
		self.logger.setLevel(level)

	def setLogLevelDebug(self):
		self.setLogLevel(logging.DEBUG)

	def setLogLevelInfo(self):
		self.setLogLevel(logging.INFO)

	def setLogLevelWarning(self):
		self.setLogLevel(logging.WARNING)

	def setLogLevelError(self):
		self.setLogLevel(logging.ERROR)
