from pyqtlib import QtCore, QtGui
import os

class FileSystemView(QtGui.QTreeView):
    """
    The FileSystemView is a QTreeView which is assumed to be connected to a QFileSystemModel.  The purpose of this View
    is to provide some common actions and menus one might want to interact with a file system.  One major drawback of
    the standard QFileSystemModel is that it can only monitor a single tree.  However, with the FileSystemView, one can
    create a folder which contains symlinks to other trees on the file system.


    """

    fileDoubleClicked = QtCore.pyqtSignal(str)    # emits the file name that was double clicked
    folderDoubleClicked = QtCore.pyqtSignal(str)  # emits the folder name that was double clicked

    def __init__(self, parent=None):
        super(FileSystemView, self).__init__(parent)


        self.doubleClicked.connect(self.itemDoubleClicked)

        ##########
        # Actions and action groups
        ##########
        self.fileActionGroup = QtGui.QActionGroup(self)
        self.viewActionGroup = QtGui.QActionGroup(self)

        expandAllAction = QtGui.QAction('Expand All', self, triggered=self.expandAll)
        collapseAllAction = QtGui.QAction('Collapse All', self, triggered=self.collapseAll)
        adjustColumnsAction  = QtGui.QAction('Adjust columns', self, triggered=self.adjustColumns)

        self.newFileAction = QtGui.QAction('New File', self, triggered=self._createFileDialog)
        self.newFolderAction = QtGui.QAction('New Folder', self, triggered=self._createFolder)

        self.deleteAction = QtGui.QAction('Delete', self, triggered=self._deleteItem)
        self.deleteAction.setEnabled(False)
        self.addLinkDialogAction = QtGui.QAction('Add Link', self, triggered=self.addLinkDialog)
        self.unlinkAction = QtGui.QAction('Unlink', self, triggered=self._unlinkSelected)

        self.fileActionGroup.addAction(self.newFolderAction)
        self.fileActionGroup.addAction(self.newFileAction)
        self.fileActionGroup.addAction(self.deleteAction)
        self.fileActionGroup.addAction(self.addLinkDialogAction)
        self.fileActionGroup.addAction(self.unlinkAction)

        self.viewActionGroup.addAction(expandAllAction)
        self.viewActionGroup.addAction(collapseAllAction)
        self.viewActionGroup.addAction(adjustColumnsAction)

    def addLinkDialog(self):
        """
        Add a symlink to the root folder.

        This method will present the user with a QFileDialog and allow them to choose another folder on the file system
        to add to the model / view.
        """
        folderName = QtGui.QFileDialog.getExistingDirectory(caption="Choose folder", options=QtGui.QFileDialog.ShowDirsOnly)
        if folderName is not None and str(folderName) != '':
            src = str(folderName)
            root = self.model().rootPath()
            dst = os.path.join(root,os.path.basename(str(folderName)))
            os.symlink(src,dst)

    def adjustColumns(self):
        """
        Convenience function to resize the column widths to the contents of the model.
        """
        for col in range(self.model().columnCount()):
            self.resizeColumnToContents(col)

    def getFileMenu(self):
        """
        Return a QMenu with all the actions from the fileActionGroup

        Returns:
            QMenu
        """
        menu = QtGui.QMenu()
        menu.addActions(self.fileActionGroup.actions())
        return menu

    def getViewMenu(self):
        """
        Return a QMenu with all the actions from the viewActionGroup
        Returns:

        """
        menu = QtGui.QMenu()
        menu.addActions(self.viewActionGroup.actions())
        return menu

    def itemDoubleClicked(self, index):
        """
        Handler for when an item is double clicked

        This method will in turn emit either the 'fileDoubleClicked' or 'folderDoubleClicked' signal

        Args:
            event: QMouseEvent
        """
        path = self.model().filePath(index)
        if self.model().isDir(index):
            self.folderDoubleClicked.emit(path)
        else:
            self.fileDoubleClicked.emit(path)

    def mousePressEvent(self, event):
        """
        Override of the base mousePressEvent.

        This allows a user to "deselect" any currently selected item.

        Args:
            event: QMouseEvent
        """
        self.clearSelection()
        QtGui.QTreeView.mousePressEvent(self, event)

    def selectionChanged(self, QItemSelection, QItemSelection_1):
        """
        Overload of the base "selecetionChanged" method.

        The purpose for overloading this method is to enable/disable the actions based on what is currently
        selected in the view.

        newFileAction and newFolderAction are only enabled if there is no selection, or the selection is a folder.

        The "deleteAction" is only enabled if a file or folder is selected, and not a symbolic link.  This will, however,
        allow a user to delete an item which is a child of the link, just not the actual link itself.

        "addLinkDialogAction" will always be enabled.

        "unlinkAction" will only be enabled if a symbolic link is selected.

        Args:
            QItemSelection:
            QItemSelection_1:
        """
        QtGui.QTreeView.selectionChanged(self, QItemSelection, QItemSelection_1)
        indexes = self.selectedIndexes()
        self.newFileAction.setDisabled(True)
        self.newFolderAction.setDisabled(True)
        self.deleteAction.setDisabled(True)
        self.unlinkAction.setDisabled(True)
        if len(indexes) == 0:
            self.newFileAction.setDisabled(False)
            self.newFolderAction.setDisabled(False)
        else:
            self.deleteAction.setDisabled(False)
            index = indexes[0]
            path = self.model().filePath(index)
            if self.model().isDir(index):
                self.newFileAction.setDisabled(False)
                self.newFolderAction.setDisabled(False)

            if os.path.islink(path):
                self.deleteAction.setDisabled(True)
                self.unlinkAction.setDisabled(False)

    @QtCore.pyqtSlot()
    def _createFileDialog(self):
        """
        Private slot connected to 'newFileAction'.
        """
        indexes = self.selectedIndexes()
        root = self.model().rootPath()
        if len(indexes) > 0:
            index = indexes[0]
            root = self.model().filePath(index)

        filename, ok = QtGui.QInputDialog.getText(self, 'Input file name', 'name')
        if ok:
            filename = str(filename)
            path = os.path.join(root,filename)
            f_ptr = open(path, 'w')
            f_ptr.close()

    def _createFolder(self):
        """
        Private slot connected to "newFolderAction"
        """
        index = self.selectedIndexes()[0]
        self.model().mkdir(index,'NewFolder')

    def _deleteItem(self):
        """
        Private slot connected to "deleteAction"
        """
        index = self.selectedIndexes()[0]
        if index is not None:
            path = self.model().filePath(index)
            msg = 'Delete "{}"?  This can NOT be undone!'.format(path)
            reply = QtGui.QMessageBox.warning(self, 'Are you sure?', msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.Cancel)

            if reply == QtGui.QMessageBox.Yes:
                if self.model().isDir(index):
                    success = self.model().rmdir(index)
                else:
                    success = self.model().remove(index)

    def _unlinkSelected(self):
        """
        Private slot connected to "unlinkAction"
        """
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            index = indexes[0]
            path = self.model().filePath(index)
            os.unlink(path)
