"""
The DictionaryWidget is a convenience widget combining the DictionaryModel and the DictionaryView

The purpose of this widget is to provide a user interface for viewing an editing Python dictionaries within a
widget.  The dictionary is displayed as a tree view in two columns.  The left column contains the keys, and
the right column displays the value.  For compound dictionaries (dictionaries which contain values which are themselves
dictionaries or lists), the tree can expand to hold subsequent "levels" of the hierarchy.  When a value is a list,
the "keys" are displayed as list indices.  The model will always return it's contents in alphabetical order by key.

This widget provides both a code API and a user context menu in order to add or remove items from the dictionary.  When
adding values, if the value is a dictionary or list, the tree will auto-matically reconfigure itself based on the update
to the data.
"""

from pyqtlib import QtCore, QtGui

from pyqtlib.model import DictionaryModel
from pyqtlib.view import DictionaryView


class DictionaryWidget(QtGui.QWidget):

    itemChanged = QtCore.pyqtSignal('QStandardItem*')

    def __init__(self, parent=None):
        super(DictionaryWidget, self).__init__(parent)
        self.model = None
        self.view  = None

        self.setView(DictionaryView())
        self.setModel(DictionaryModel())

        self.setHeaderLabels(['name', 'value'])

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.view)
        self.setLayout(layout)

    def addItem(self, index):
        """
        Add a new key-value line into the model

        Args:
            index: QModelIndex of the parent. If None, this will be a root item
        """
        item = self.model.itemFromIndex(index)
        self.model.addItem(header='item', value='', parentItem=item)

    def adjustColumns(self):
        """
        Resize the columns to fit the data
        """
        for col in range(self.model.columnCount()):
            self.view.resizeColumnToContents(col)

    def clear(self):
        self.model.clear()

    def getDictionary(self):
        """
        Get the contents of the model as a Python dictionary

        Returns:
            Model data as a Python dictionary
        """
        return self.model.getDictionary()

    def getValue(self, keys):
        """
        Get a value from a list of indexes.

        Example:
            data = {'a' : 1,
                    'b' : 2,
                    'c' : ['x','y','z']}


        getValue(['c',2]) will return 'z'

        Args:
            keys: list of keys for the data

        Returns: value
        """
        return self.dataModel.getValue(keys)

    def removeItem(self, index):
        """
        Remove a key-value pair by index.  NOTE: If this item has children, the children will also be removed.

        Args:
            index: QModelIndex of the item to remove
        """
        item = self.model.itemFromIndex(index)
        self.model.removeItem(item)

    def setHeaderLabels(self, values):
        """
        Set the header values.

        Args:
            value: list of strings to be displayed in the header
        """
        self.model.setHorizontalHeaderLabels(values)

    def setModel(self, model):
        """
        Set the DictionaryModel for this widget.

        Args:
            model: A pyqtlib.model.DictionaryModel instance
        """
        self.model = model
        if self.view is not None:
            self.view.setModel(self.model)
        self.model.itemChanged.connect(self.itemChanged)
        self.model.modelAboutToBeReset.connect(self.view.cacheColumnWidths)

    def setDictionary(self, value):
        """
        Set the model's dictionary.

        Args:
            value: Python dictionary
        """
        self.model.setDictionary(value)

    def setValue(self, keys, value):
        """
        Set a value in the dictionary

        Args:
            keys: list of indexes
            value: value to set

        Example:
            data = {'a' : 1,
                    'b' : 2,
                    'c' : ['x','y','z']}

        setValue(['c',2], 4)

            data = {'a' : 1,
                    'b' : 2,
                    'c' : ['x','y',4]}
        """
        self.model.setValue(keys, value)

    def setView(self, view):
        """
        Set the view for this widget.

        Args:
            view: Instance of a pyqtlib.view.DictionaryView
        """
        self.view = view
        if self.model is not None:
            self.view.setModel(self.model)

        self.view.addItemRequest.connect(self.addItem)
        self.view.removeItemRequest.connect(self.removeItem)
