from pyqtlib import QtGui, QtCore
from .object_info import getAttributes

atomic_types = [str, float, bool, int, unicode]


class ObjectTreeWidget(QtGui.QTreeWidget):
    """
    A tree view widget to display the current value of an arbitrary python object

    The ObjectTreeWidget will display the "public" attributes of any arbitrary
    python object. To update an object in the treeview, simply call the "update"
    method, passing in a unique name and the object to be displayed.  If the object
    does not already exist, it will be added to the view.
    """

    def __init__(self,parent=None):
        super(ObjectTreeWidget, self).__init__(parent)

        # Actions
        self.expandAllAction = QtGui.QAction('&Expand All',self)
        self.collapseAllAction = QtGui.QAction('&Collapse All',self)
        self.adjustColumnsAction = QtGui.QAction('&Adjust Columns',self)
        self.viewAllAction = QtGui.QAction('&View All',self)

        self.expandAllAction.triggered.connect(self.expandAll)
        self.collapseAllAction.triggered.connect(self.collapseAll)
        self.adjustColumnsAction.triggered.connect(self.adjustColumns)
        self.viewAllAction.triggered.connect(self.viewAll)

        #Right-click menu
        self.contextMenu = QtGui.QMenu(self)
        self.contextMenu.addAction(self.expandAllAction)
        self.contextMenu.addAction(self.collapseAllAction)
        self.contextMenu.addAction(self.adjustColumnsAction)
        self.contextMenu.addAction(self.viewAllAction)

        # Set the context menu policy for the right-click pop-up menu
        self.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)

        self.setColumnCount(3)
        self.setHeaderLabels(['name','type','value'])
        self.setSortingEnabled(False)
        #self.sortItems(0,QtCore.Qt.AscendingOrder)


    def updateView(self, name, obj):
        """
        Update the value of an object

        @param name Name of the object.  All objects must have a unique name.
        @param obj Current value of the object.
        """
        item = self.findItems(name, QtCore.Qt.MatchExactly)
        if len(item) == 0:
            item = QtGui.QTreeWidgetItem()
            self.addTopLevelItem(item)
        else:
            item = item[0]
        self._updateClass(name, obj, item)

    def getContextMenu(self):
        return self.contextMenu

    def contextMenuEvent(self,event):
        """
        This is the right-click pop-up menu actions

        @param event The event
        """
        self.contextMenu.exec_(QtGui.QCursor.pos())

    @QtCore.pyqtSlot()
    def adjustColumns(self):
        for col in range(self.columnCount()):
            self.resizeColumnToContents(col)

    @QtCore.pyqtSlot()
    def viewAll(self):
        self.expandAll()
        self.adjustColumns()

    def _updateClass(self, name, obj, item):
        """
        Recursively update the attributes of a class
        @param name Name of this class attribute
        @param obj Value of this attribute
        @param item The QtGui.QTreeWidgetItem containing the value for this attribute
        """

        item.setData(0, QtCore.Qt.DisplayRole, name)
        item.setData(1, QtCore.Qt.DisplayRole, obj.__class__.__name__)

        if type(obj) in atomic_types:
            item.setData(2, QtCore.Qt.DisplayRole, obj)

        elif type(obj) in [list, tuple]:
            self._updateList(name, obj, item)

        elif type(obj) is dict:
            self._updateDict(name, obj, item)

        else:
            attr = getAttributes(obj)
            for k,v in attr.items():
                c = self._findChild(k,item)
                if c == None:
                    c = QtGui.QTreeWidgetItem()
                    c.setData(0, QtCore.Qt.DisplayRole, k)
                    c.setData(1, QtCore.Qt.DisplayRole, v.__class__.__name__)
                    item.addChild(c)
                if type(v) in [list, tuple]:
                    self._updateList(k, v, c)
                elif type(v) is dict:
                    self._updateDict(k, v, c)
                else:
                    self._updateClass(k, v, c)

    def _updateList(self, name, obj, item):
        """
        Update a list attribute
        @param name Name of this list attribute
        @param obj Value of this attribute.  This should be a list.
        @param item The QtGui.QTreeWidgetItem containing the value for this attribute
        """
        if len(obj) != item.childCount():
            item.takeChildren()
            for i in range(len(obj)):
                c = QtGui.QTreeWidgetItem()
                item.addChild(c)
        for i in range(len(obj)):
            self._updateClass(i, obj[i], item.child(i))

    def _updateDict(self, name, obj, item):
        """
        Update a dictionary attribute
        @param name Name of this dictionary attribute
        @param obj Value of the dictionary.
        @param item The QtGui.QTreeWidgetItem containing the value for this attribute
        """
        if not isinstance(obj, dict):
            raise Exception('Trying to update a dictionary with a non-dictionary object')

        if len(obj) != item.childCount():
            item.takeChildren()
            for i in range(len(obj)):
                c = QtGui.QTreeWidgetItem()
                item.addChild(c)
        for k,i in zip(sorted(obj),range(len(obj))):
            self._updateClass(k, obj[k], item.child(i))

    def _findChild(self, name, parent):
        """
        Find the child item of a parent by name
        @param name Name of the item
        @param parent The parent item for the search
        """
        for i in range(parent.childCount()):
            if parent.child(i).data(0,QtCore.Qt.DisplayRole) == name:
                return parent.child(i)
        return None
