from operator import attrgetter, itemgetter
import inspect

def getAttributes(obj, publicOnly=True):
    '''
    Get attribute information about an object.

    This function will return a dictionary of an object's attributes
    and their corresponding values.  By default, only attributes that
    are "public" are returned.  "Public" attributes are those that do not
    start with a '_'.  Attributes are all members that are not callable.
    '''
    attributes = {}
    members = dir(obj)
    for memberName in members:
        if hasattr(obj, memberName):
            member = getattr(obj, memberName)
            if not callable(member):
                if publicOnly:
                    if not memberName.startswith("_"):
                        attributes[memberName] = member
                else:
                    attributes[memberName] = member
    return attributes

def getAttributeTree(name, obj, delimiter='.', publicOnly=True):
    '''
    Get a list of all atomic attributes

    This function will recursively search the object and return a list of
    all attributes (and sub-attributes) of all members that have a value.  By
    default, levels of the hierarchy are delimiter with a ".".

    @param name Name of the object
    @param obj The instance of the object
    @param delimiter The character or string to denote levels in the hierarchy
    @param publicOnly Switch to determine if only public methods will be shown
    @return List of atomic attributes in the object's hierarchy
    '''
    attributeList = []

    if type(obj) in [str, float, bool, int]:
        attributeList.append(str(name))
        return attributeList
 
    if type(obj) in [tuple, list]:
        if len(obj) == 0:
            attributeList.append(str(name))
            return attributeList
        else:
            for i in range(len(obj)):
                n = name + delimiter + str(i)
                attributeList = attributeList + getAttributeTree(n, obj[i], delimiter,publicOnly)
            return attributeList

    for k,v in getAttributes(obj, publicOnly).items():
        n = name + delimiter + k
        attributeList = attributeList + getAttributeTree(n,v,delimiter,publicOnly)

    return sorted(attributeList)

def getAttributeValue(obj, attributeString, delimiter='.'):
    '''
    Get the value of a nested attribute in a string

    This function allows you to get an attribute of a complex object
    (for example a class with attributes which are also classes, lists, etc)
    @param obj The object.
    @param attributeString The "path" of the attribute within the object
    @param delimiter The delimiter within the attributeString to denote hierarchy
    '''
    attrString = attributeString.replace(delimiter,'.')
    attr_split = attrString.split('.')
    attr_obj = obj
    for attr in attr_split:
        try:
            attr_obj = attr_obj[int(attr)]
        except ValueError as e:
            if hasattr(attr_obj, attr):
                attr_obj = getattr(attr_obj, attr)
    return attr_obj

def getDefString(obj):
    """
    Return a string of a Python function or member method
    definition.

    @type obj function or method
    @param obj Python callable.

    @type return string
    @return String showing the function/method definition
    """
    if not callable(obj):
        raise Exception('{} is not callable'.format(obj))
    sourceLines = inspect.getsourcelines(obj)[0]
    raw_def = sourceLines[0]
    def_line = raw_def.strip()
    if def_line.startswith('def'):
        def_line = def_line[3:]
    def_line = def_line.strip().rstrip(':')
    def_line = def_line.replace("self, ", "")
    def_line = def_line.replace("self,", "")
    def_line = def_line.replace("self", "")

    return def_line


class ObjectInfo(object):
    '''
    Class to encapsulate a class and return various information.
    '''
    def __init__(self, obj):
        self.methods    = {}
        self.attributes = {}
        self.setObject(obj)

    def setObject(self, obj):
        self.methods    = {}
        self.attributes = {}
        self._obj = obj
        members = dir(self._obj)
        for memberName in members:
            if hasattr(self._obj, memberName):
                member = getattr(self._obj, memberName)
                if callable(member):
                    self.methods[memberName] = member
                else:
                    self.attributes[memberName] = member

    def getMethods(self, public=True):
        methods = {}
        if public:
            for name, val in self.methods.items():
                if not name.startswith("_"):
                    methods[name] = val
            return methods
        else:
            return self.methods

    def getAttributes(self, public=True):
        attributes = {}
        if public:
            for name, val in self.attributes.items():
                if not name.startswith("_"):
                    attributes[name] = val
            return attributes
        else:
            return self.attributes

    def getObject(self):
        return self._obj

    def setAttribute(self, name, value):
        self.attributes[name] = value

    def callMethod(self, name, *args, **kwargs):
        self.methods[name](*args,**kwargs)

    def getAttribute(self, name):
        return self.attributes[name]

    def getAttributeNames(self, public=True):
        return self.getAttributes(public).keys()

    def getMethodNames(self, public=True):
        return self.getMethods(public).keys()

    def getMethodDefinitionStrings(self, public=True):
        methodStrings = []
        methods = self.getMethods(public)
        for method in methods.values():
            methodStrings.append(getDefString(method))
        return methodStrings