# @file setup.py
# @copyright Copyright 2017 NASA. All Rights Reserved.

from __future__ import print_function
from catkin_pkg.python_setup import generate_distutils_setup
import os
import sys

if 'ROS_DISTRO' in os.environ:
    from distutils.core import setup
else:
    from setuptools import setup

d = generate_distutils_setup(
    packages=[
        'pyqtlib',
        'pyqtlib.dialog',
        'pyqtlib.log',
        'pyqtlib.model',
        'pyqtlib.node_graph',
        'pyqtlib.plotter',
        'pyqtlib.stylesheet',
        'pyqtlib.table',
        'pyqtlib.threads',
        'pyqtlib.tree',
        'pyqtlib.view'],
    package_dir={'': 'src'},
    install_requires=[
        'numpy',
        'pyqtgraph',
        'nasa_common_logging'
# The following dependencies are required but there is no way to get them
# through the python ecosystem. Please use your platform package manager to
# install them.
#        'qt4',
#        'sip',
    ]
)

try:
    from nasa_common_pkg.pep440 import update_setup
    d = update_setup(d)
except ImportError:
    print('Unable to find nasa_common_pkg, package will not include repo details', file=sys.stderr)

setup(**d)
