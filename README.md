NASA PyQt Library
===================

## Overview

This repository is for PyQt related widgets and functions.  Although this repository is structured as
a catkin package, no items within this repository are meant to have dependencies on ROS.  The goal is
to provide a library of widgets for use in as many different projects and applications as possible, with
the least amount of dependencies
     

## Packages

- [log](#log) - Widgets related to event logging
- [plotter](#plotter) - Widgets related to plotting and graphing data
- [table](#table) - Table widgets
- [tree](#tree) - Tree widgets


## log

### LogWindow
The `LogWindow` is a QWidget provides an HTML formatted text box that will listen to logged data.  The `LogWindow` object
will add the [`LogWindowHandler`](#LogWindowHandler) as a handler to the Python logging infrastructure.  This widget supports setting the
log-level directly, and will color the text based on the log level.

### LogWindowHandler
The `LogWindowHandler` is a handler for Python's built-in `logging` module.  This widget/handler hybrid emits
logged events as a signal which can then be re-directed to other widgets.

### LogTableWidget
The `LogTableWidget` has a `QTableView` that can display log-like information.  This widget does not actually depend on logging
directly, it is simply a setup for displaying event log messages.  It uses a custom `QStandardItemModel` (`LogModel`) to facilitate
coloring.  This widget also provides filtering options by log-level.  Unlike the [`LogWindow`](#LogWindow), this widget filters the information
through a `QSortFilterProxy` and does NOT change the log level.

### LogFileView
The `LogFileView` is an extension of the [`LogTableWidget`](#LogTableWidget).  This widget will follow log files and add elements to the display when
one of those files change.  It functions similarly to the "tail" command in Linux (with the the -f option to follow).


## plotter

### TimeSeriesPlotWidget
This widget in a used for displaying time-series data in a two-dimensional plot.  Data points for each series of data will be joined together
using a straight line.


## tree

### PathWidget
The `PathWidget` is a `QTreeView` for path-like data items.  If the widget is instantiated with `checkable = True`,
an event will be emitted with that item and it's checkstate.  Every sub-item will also be checked or unchecked to
match the parent while also emitting a signal.  In this fashion, one can select or de-select many items in the tree
at once.

### PathDataWidget
An extension of the PathWidget which also contains multiple columns of data.  The data for any path is treated like a dictionary.

