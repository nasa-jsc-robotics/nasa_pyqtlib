Change Log
==========

4.4.0
-----

* Removed trusty ci
* `GraphView` - Added override so that middle click and drag also allows users to pan 
* `DictionaryModel` - Auto generate unique keys if necessary
* `DictionaryModel` - Fix crash when removing last item from dictionary
* Consolidate redundant DictionaryModel tests
* Added MultiStyleConnection that allows for straight lines, orthogonal lines, and Bezier curves in the node_graph module.

4.3.0
-----

* Added DictionaryModelItem
* Added expanded state caching and column width caching to keep the view from collapsing after editing a DictionaryWidget
* Added better support for setting header labels in the DictionaryWidget

4.2.1
-----

* Fixed bug when deleting node_graph.Node.Node objects
* Fixed wheel event bug introduced in PyQt5
* Made elements of Ports non-selectable by default

4.2.0
-----

* Mod so that the dictionary model casts edit role values as a string.  This allows ints to turn into floats
* Allow arbitrary format string for display of value items

4.1.1
-----

* Fixed Qt5-related signal bug for `node_graph.GraphScene` class

4.1.0
-----

* Added feature to set color of cells in a `PathModel` and `PathWidget`

4.0.0
-----

* Enabled catkin run tests.
* Make selection of `pyqt-dev-tools` dependent on rosdep to select correct version for OS.
* Removed dependency on `Qsci`.
* Added `QtTest` to pyqtlib imports.
* Node removed `scene` parameter.
* `scene` parameter for `Port`, `RequiresPort`, `TextItem`, `BoxItem`, `Element`, and `Node` classes.
* `GraphView` - Converted `itemDoubleClicked` signal type to `string` of class.

3.0.5
-----

* Changed test file extension from `.block` to `.group`

3.0.4
-----

* Fixed bug in `plotter/TimeSeriesPlotWidget.py` which broke `smtlogviewer`.
